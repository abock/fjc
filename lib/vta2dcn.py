#!/usr/bin/env python
# module load aug_sfutils
import matplotlib.pyplot as plt
import numpy as np
import sys
from scipy.interpolate import interp1d, griddata #, Rbf, PchipInterpolator
from aug_sfutils import SFREAD, EQU, rz2rho
from IPython import embed


channels = [0,1,2,4,5]

class Bunch(object):
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

def vta2dcn(vta, dcn, eq, with_DCN=False, DCN_avg_int=200, DCN_downsampled=True, channels=channels,
    **kwargs):
    """
    Generate DCN time traces from VTA data.

    Input:

        vta: opened VTA SFREAD()
        dcn: opened DCN/K SFREAD()
        eq: opened EQU (previously map_equ.equ_map())

    Output:

        dictionary with time traces for each H-%i in DCN
    """
    Nec = Bunch(data=vta.getobject('Ne_c'), time=vta.gettimebase('Ne_c'))
    Rc = vta.getobject('R_core')
    Zc = vta.getobject('Z_core')
    if 'DCNgeo' in dcn.getlist():
        dcngeo = dcn.getparset('DCNgeo')
    else:
        dcngeo = None

    t_inds = Nec.time < eq.time[-1] # no points outside equil
    t_inds *= (np.average(Nec.data, axis=1) > 0) # no points without data
    times = Nec.time[t_inds]

    z,r = np.meshgrid(Zc,Rc[t_inds])
    
    rho = rz2rho(eq, r, z, t_in=times, coord_out='rho_pol', extrapolate=False)

    points = []
    values = []
    for r, ne, t in zip(rho, Nec.data[t_inds], times):
        points += list(np.column_stack((r, [t]*len(r))))
        values += list(ne)
    points = np.array(points)
    values = np.array(values) #np.clip(, 0, 2e20)


    min_t = eq.time.min()+0.1
    max_t = eq.time.max()-0.1
    t_grid = 200
    N_r = 1000
    N_t = int((max_t - min_t)*t_grid)
    grid_r, grid_t = np.mgrid[0:1:N_r*1j, min_t:max_t:N_t*1j]
    if 'method' not in kwargs:
        grid_z1 = griddata(points, values, (grid_r, grid_t), method='nearest', **kwargs)
    else:
        grid_z1 = griddata(points, values, (grid_r, grid_t), **kwargs)
    grid_z1 = np.clip(grid_z1, 0, np.mean(grid_z1)*3)
    grid_z1 = np.clip(grid_z1, 0, np.mean(grid_z1, axis=0)*3)


    r = np.linspace(0,1,grid_z1.shape[0])

    HRin, HRaus = {}, {}
    Hzin, Hzaus = {}, {}
    HRs, Hzs = {}, {}
    Hrhos = {}
    ds = {}

    if dcngeo is None:
        HRaus[1], Hzaus[1], HRin[1], Hzin[1] = 2.1664e3,  0.1533e3, 1.0060e3,  0.1447e3
        HRaus[2], Hzaus[2], HRin[2], Hzin[2] = 2.1695e3,  0.3236e3, 1.0067e3,  0.3154e3
        HRaus[0], Hzaus[0], HRin[0], Hzin[0] = 2.1614e3, -0.1603e3, 1.0065e3, -0.1465e3
        HRaus[4], Hzaus[4], HRin[4], Hzin[4] = 2.1668e3,  0.1528e3, 1.1287e3,  1.0566e3
        HRaus[5], Hzaus[5], HRin[5], Hzin[5] = 2.1715e3,  0.4426e3, 1.0946e3,  0.8032e3

    for i in channels:
        #print i
        if dcngeo is not None:
            HRin[i]  = dcngeo['H%iHFS_R'%i]
            HRaus[i] = dcngeo['H%iLFS_R'%i]
            Hzin[i]  = dcngeo['H%iHFS_z'%i]
            Hzaus[i] = dcngeo['H%iLFS_z'%i]
        HRs[i] = np.linspace(HRin[i], HRaus[i], int(HRaus[i] - HRin[i]))
        Hzs[i] = np.linspace(Hzin[i], Hzaus[i], int(HRaus[i] - HRin[i]))
        ds[i] = np.sqrt((HRs[i][1] - HRs[i][0])**2 + (Hzs[i][1]-Hzs[i][0])**2)/1e3
        Hrhos[i] = rz2rho(eq, HRs[i]/1e3, Hzs[i]/1e3, t_in=times, coord_out='rho_pol', extrapolate=False) # rz2rho(HRs[i]/1e3, Hzs[i]/1e3, times)


    vta_modelled = {i:[] for i in channels}
    times_modelled = []
    for ti, t in enumerate(times):
        if t < min_t: continue
        if t >= max_t: break
        try:
            r2n = interp1d(r, grid_z1[:,int((t-min_t)*t_grid)], bounds_error=False, fill_value=0)
            for h in channels:
                cr = Hrhos[h][ti]
                hr = r2n(cr)
                vta_modelled[h].append(sum(hr*ds[h]))
            times_modelled.append(t)
        except:
            continue

    times_modelled = np.array(times_modelled)
    vta_modelled = {i:(times_modelled, np.array(vta_modelled[i])) for i in vta_modelled}

    if with_DCN:
        for h in vta_modelled:
            dcnh = dcn.getobject('H-%i'%h)
            dcnh.data -= np.average(dcnh.data[:DCN_avg_int]) if DCN_avg_int > 0 else 0
            vta_modelled[h] += (times_modelled, interp1d(dcnh.time, dcnh.data)(times_modelled)) \
                               if DCN_downsampled else (dcnh.time, dcnh.data)

    return vta_modelled

if __name__ == '__main__':
    #shot =  33889 # 33870 #
    if len(sys.argv) > 1:
        try:
            shot = int(sys.argv[1])
        except:
            pass


    shot = globals()['shot'] if 'shot' in globals() else int(input('Which shot? '))
    if False or 'eq' not in globals().keys() or 'lastshot' not in globals() or globals()['lastshot'] != shot:
        lastshot = shot
        vta = SFREAD('VTA', shot)
        eq = EQU(shot)
        try:
            dcn = SFREAD('DCN', shot)
            dcnh = {i:Bunch(data=np.array(dcn.getobject('H-%i'%i)), time=dcn.gettimebase('H-%i'%i)) for i in channels}
        except:
            pass
        try:
            dck = SFREAD('DCK', shot)
            dckh = {i:Bunch(data=np.array(dck.getobject('H-%i'%i)), time=dcn.gettimebase('H-%i'%i)) for i in channels}
        except:
            pass

        for h in dcnh:
            dcnh[h].data -= np.average(dcnh[h].data[:200])
            if 'dckh' in globals():
                dckh[h].data -= np.average(dckh[h].data[:200])
        vta_modelled = vta2dcn(vta, dcn, eq)
        if vta_modelled is None:
            sys.exit()


    plt.figure(1)
    plt.title('DCN from VTA for #%i'%shot)
    plt.clf()
    plt.grid()

    to_save = np.zeros((len(channels)+1, len(vta_modelled[0][0])))
    to_save[0] = vta_modelled[0][0]
    for i, h in enumerate(channels):
        offset = h*1e20
        tmp, = plt.plot(dckh[h].time, dckh[h].data+offset, alpha=0.3)
        plt.plot(vta_modelled[h][0], vta_modelled[h][1]+offset, '--', color=tmp.get_color())
        to_save[i+1] = vta_modelled[h][1]

    if 'save' in sys.argv:
        np.savetxt('vta_dcn_%i.dat'%shot, to_save.T, fmt=['% 5e']*to_save.shape[0])

    plt.show()
