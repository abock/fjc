#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import time
from scipy import signal
from scipy.interpolate import interp1d
import sys
from .wrappedkalman import WrappedKalman
#import h5py
#import ww_local as ww
from matplotlib.widgets import MultiCursor

dcn_ne_fringe = 0.572e19

def butter_bandpass(lowcut, highcut, Fs, order=2):
    nyq = 0.5 * Fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = signal.butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, Fs, order=2):
    b, a = butter_bandpass(lowcut, highcut, Fs, order=order)
    y = signal.filtfilt(b, a, data)
    return y

def butter_lowpass(highcut, Fs, order=2):
    nyq = 0.5 * Fs
    #low = lowcut / nyq
    high = highcut / nyq
    b, a = signal.butter(order, high, btype='low')
    return b, a

def butter_lowpass_filter(data, highcut, Fs, order=2):
    b, a = butter_lowpass(highcut, Fs, order=order)
    y = signal.filtfilt(b, a, data)
    return y




def eval_dcn_old(t, data, stride=50):
    Fs = np.average(1./np.diff(t))
    ref = data[0]-np.average(data[0])
    ref = butter_bandpass_filter(ref, 9e3, 11e3, 500e3)
    analytic_signal = signal.hilbert(ref)
    amplitude_envelope = np.abs(analytic_signal)
    ref /= amplitude_envelope


    sinref = ref
    cosref = np.imag(signal.hilbert(sinref))


    new_dcns = []
    intensities = []
    offsets = []
    cutoffs = [13.5e3, 14.0e3, 13.5e3, 14.5e3, 13.5e3]
    for i, channel in enumerate(data[1:]):
        channel -= np.average(channel)
        channel = butter_lowpass_filter(channel, cutoffs[i], 500e3, order=5)
        analytic_signal = signal.hilbert(channel)
        amplitude_envelope = np.abs(analytic_signal)

        channel /= amplitude_envelope

        a = np.cumsum(sinref*channel)
        b = np.cumsum(cosref*channel)

        da = a[stride::stride]-a[:-stride:stride]
        db = b[stride::stride]-b[:-stride:stride]

        new_t = t[:-stride:stride]

        phases = np.arctan2(db, da)

        intens = np.sqrt(db**2+da**2)/(stride/2)
        intensities.append(intens)

        to_plot = np.unwrap(phases)/(2*np.pi)
        offset = np.average(to_plot[new_t < 0])
        offsets.append(offset)
        to_plot -= offset


        #dcn_data = dcn('H-%i'%(dcn_channels[i]))

        #to_plot2 = dcn_data.data/dcn_ne_fringe
        #to_plot2 -= np.average(to_plot2[dcn_data.time < 0.03])

        new_dcns.append(to_plot)

    new_dcns = np.array(new_dcns)
    dci = intensities = np.array(intensities)

    return new_t, new_dcns, dci, offsets



from numba import jit, njit
from numba.typed import Dict

@jit
def process_reference(lt, ls, last_w=None, last_p=None):
    """Function processing the reference signal chunk. Returns amplitude, angular frequency and phase."""
    lsm = np.mean(ls)    
    def moving_average(data, N):
        weights = np.ones(N) / N
        return np.convolve(data, weights, mode='same')

    dt = np.mean(np.diff(lt))
    ls = moving_average(ls, int(0.02e-3/dt)) # 2e-5s for 10kHz
    
    zero_crossing_is = []
    first_crossing_down = None
    for j in range(1,len(ls)):
        if (ls[j] >= lsm and ls[j-1] <= lsm) or (ls[j] <= lsm and ls[j-1] >= lsm):
            if len(zero_crossing_is) == 0:
                first_crossing_down = 0 if (ls[j] >= lsm and ls[j-1] <= lsm) else 1
            zero_crossing_is.append(j)
    zero_crossing_is = np.array(zero_crossing_is)        
    A = np.std(ls) * np.sqrt(2)
    w = 2 * np.pi / (np.median(np.diff(zero_crossing_is))*2 * np.median(np.diff(lt)))
    p = - np.pi * zero_crossing_is[0] / np.median(np.diff(zero_crossing_is)) - first_crossing_down*np.pi
    return (A, w, p % (2*np.pi))

@jit
def process_signal(lt, ls, w, granularity=27):
    A = np.std(ls) * np.sqrt(2)
    def func(p):
        return A*np.sin(w*(lt - lt[0]) + np.pi/180*p)
        
    l1ns, ps = np.zeros(len(range(0, 360, granularity))), np.zeros(len(range(0, 360, granularity)))
    for i, p in enumerate(range(0, 360, granularity)):
        l1n = ls - func(p)
        l1ns[i] = float(np.sum(np.abs(l1n)))
        ps[i] = p
    
    jj = l1ns.argsort()[:3]
    min_p = (ps[jj[1]]+360)%360-360
    max_p = (ps[jj[2]]+360)%360-360
    if min_p > max_p: min_p, max_p = max_p, min_p
    
    l1ns2, ps2 = np.zeros(len(range(min_p, max_p))), np.zeros(len(range(min_p, max_p)))
    for i, p in enumerate(range(min_p, max_p)):
        l1n = ls - func(p)
        l1ns2[i] = float(np.sum(np.abs(l1n)))/A/len(lt)
        ps2[i] = p

    return (A, ps2[l1ns2.argmin()]*np.pi/180 % (2*np.pi), l1ns2[l1ns2.argmin()]) 

channels = [0,1,2,4,5]

@jit
def process_shot(t, ref, data, dt=0.2e-3, chs=np.array(channels)):
    di = int(np.round(dt/np.median(np.diff(t))))
    step = int(di)
    nc = len(chs)
    nr = len(ref)
    nd = int(nr/di)
    phas = np.zeros((nd, nc+1))
    amps = np.zeros((nd, nc+1))
    l1ns = np.zeros((nd, nc))
    ts = np.zeros(nd)
    for i, i0 in enumerate(range(0, nr, step)):
        ts[i] = t[i0+di-1]
        A, w, p = process_reference(t[i0:i0+di], ref[i0:i0+di])
        phas[i, -1] = p
        amps[i, -1] = A
        for j, ch in enumerate(chs):
            A, p, l1n = process_signal(t[i0:i0+di], data[ch][i0:i0+di], w)
            amps[i, j] = A
            phas[i, j] = p
            l1ns[i, j] = l1n
    return ts, phas, amps, l1ns

def eval_dcn(t, ref, data, chs=np.array(channels), cov1=1e-2, cov2=1e-3, x0=0., exp=1):
    print('begin phase fit')
    ts, phas, amps, l1ns = process_shot(t, ref, data, chs=chs)
    print('done phase fit')
    print("Kalman filter...")

    dens = np.zeros((len(ts), len(channels)))
    for i in range(5):
        dens[:,i] = np.unwrap(phas[:,-1]-phas[:,i])

    dt = np.median(np.diff(t))

    dens = np.zeros((len(channels), len(ts)))
    for i in range(len(channels)):
        dens[i] = np.unwrap(phas[:,-1] - phas[:,i]) 
        #dens[i] /= (2*np.pi)
        dens[i] -= np.median(dens[i,ts<0])

    x0 = x0 # initial state
    exponent = exp
    var = 1 # 1e-2 # 1./np.median(l1n[:,i]**exponent) # measurement noise
    cov = np.matrix([[cov1,   0  ], 
                    [  0,  cov2]]) # process noise 
    dt = dt # 2e-6 #timestep

    vd = 1
    
    wkp = []
    vs = []
    for i in range(len(channels)):        
        km = WrappedKalman(x0, dt, cov, var)
        v = []
        for d, l in zip(dens[i]%(2*np.pi), l1ns[:,i]):
            km.step(d, var=var*l**exponent)#, velocity_decay=vd)
            v.append(var*l**exponent)        
        wkp.append( np.array(km.predictions)[:-1,:,:] )
        vs.append(v)        
    wkp = np.array(wkp)
    vs = np.array(vs)

    return ts, np.unwrap((wkp[:,:,0,0].T - np.median(wkp[:,ts<0,0,0], axis=1)).T) / (2*np.pi), vs


if __name__ == "__main__":


    from aug_sfutils import SFREAD

    shot = 42042 if 'shot' not in globals() else globals()['shot'] # 33889
    dcn_channels = channels 

    if 'con' not in globals() or 'last_shot' not in globals() or shot != globals()['last_shot']:
        con = SFREAD('CON', shot)

        if False:
            signals = ['DCN_ref'] + ['DCN_H-%i'%i for i in dcn_channels]
            data = []
            data.append(con.gettimebase('DCN_ref'))
            for i, sig in enumerate(signals):
                data.append(con.getobject(sig))
            data = np.array(data)
            new_data = data.copy()

        ref = con.getobject('DCN_ref')
        t = con.gettimebase('DCN_ref')
        data = Dict()
        for i in channels:
            data[i] = np.array(con.getobject('DCN_H-%i'%i))
        last_shot = shot

    print('Calculating...')
    begin = time.time()
    ts, dens, vs = eval_dcn(t, ref, data)
    print(time.time()-begin)



    plt.plot(ts, dens.T)

    sys.exit()














    fig = plt.figure(1, figsize=(16,12))
    dcn = SFREAD('DCN', shot)
    axes = []
    for i in range(5):
        axes.append(plt.subplot(5,3,3*i+1, sharex=plt.gca()))

        
        dcnt = dcn.gettimebase('H-%i'%channels[i])
        dcny = dcn.getobject('H-%i'%channels[i])
        plt.plot(dcnt, dcny/0.572e19, lw=1, alpha=0.5, color='red')#, color=color)

        dens = phas[:,-1] - phas[:,i]
        dens = np.unwrap(dens)/(2*np.pi)
        dens -= np.median(dens[ts<0])
        color = plt.plot(ts, dens, label=channels[i], lw=1, alpha=0.65)[0].get_color()

        udens = np.unwrap(wkp[i,:,0,0])/ 2 / np.pi
        udens -= np.median(udens[ts<0])
        plt.plot(ts, udens, '.-', ms=2, lw=1)

        plt.ylim(udens.min() - 1, udens.max() + 1)
        
        plt.grid()

        axes.append(plt.subplot(5,3,3*i+2, sharex=plt.gca()))
        plt.plot(ts, dens%1, label=channels[i], lw=1, alpha=0.65)
        plt.plot(ts, udens%1)
        #plt.plot(ts, wkp[i,:,0,0])
        plt.plot(ts, wkp[i,:,1,0])
        
        plt.ylim(-0.3,1.1)
        plt.grid()
        
        axes.append(plt.subplot(5,3,3*i+3, sharex=plt.gca()))
        
        #plt.plot(t, data[channels[i]], lw=1)
        plt.plot(ts, vs[i], lw=1)
        #plt.gca().set_yscale('log') 
        plt.grid()   
    
    plt.tight_layout(); cursor = MultiCursor(fig.canvas, axes, color='r',lw=0.5, horizOn=False, vertOn=True)























    sys.exit()


    new_t, new_dcns, dci, offsets = eval_dcn_old(new_data[0], new_data[1:])

    colors = ['black', 'red', 'blue', 'orange', 'purple']


    indices = np.arange(len(new_t))


    plt.figure('%i'%shot)
    plt.clf()

    nrows, ncols = 6, 1
    ax = plt.subplot(nrows, ncols, 1)
    #plt.grid(True)
    for i, color in enumerate(colors):
        #ax.plot(new_t, new_dcns[i], color=color, alpha=0.3)
        #    alpha=float(fnum+1)/len(filterings))
        #plt.plot(new_t, new_dcns[i], '.', color=color,
        #    alpha=float(fnum+1)/len(filterings), markersize=3)
        plt.subplot(nrows, ncols, 1+1*i, sharex=ax)
        plt.plot(new_t, new_dcns[i], color=color)
        intens = dci[i]
        mask = intens < 0.7
        for index in indices[mask]:
            #plt.axvline(new_t[index], color='grey', alpha=0.25)
            pass
        plt.grid(True)

        plt.subplot(nrows, ncols, 6, sharex=ax)
        plt.plot(new_t, new_dcns[i], color=color)
        plt.grid(True)

        #plt.subplot(nrows, ncols, 2+2*i, sharex=ax)
        #plt.plot(new_t, dci[i], color=color)
        #plt.plot(new_t[1:], np.diff(new_dcns[i]), color=color)
        #plt.grid(True)

    plt.tight_layout()

    def write_to_AUG_DCK():
        dest = ww.shotfile()
        dest.Open(experiment="AUGD", diagnostic='DCK', shotnumber=shot)
        tvec = new_t
        dest.SetTimebase('time', tvec.astype(np.float32)[:100000])
        for i, ch in enumerate([1,2,0,4,5]):
            dest.SetSignal('H-%i'%ch, (new_dcns[i]*dcn_ne_fringe).astype(np.float32)[:100000])
            parameter = 'Offset'
            #print i, ch, offsets[i]*dcn_ne_fringe
            dest.SetParameter('ParH-%i'%ch, parameter, np.float32(offsets[i]*dcn_ne_fringe))
        dest.Close()
