#  -*- coding: utf-8 -*-
# *****************************************************************************
#
# Module authors:
#   Bernhard Sieglin <bernhard.sieglin@ipp.mpg.de>
#   Alexander Lenz <alexander.lenz@ipp.mpg.de>
#
# *****************************************************************************

import ctypes
import warnings
import datetime
from inspect import stack
from copy import copy
from ctypes import c_uint16, c_int32, c_uint32, c_float, c_void_p

import numpy

from ddww.lib import lib as libddww

warnings.simplefilter('always', DeprecationWarning)

# from IPython import embed

## \cond
# instructs doxygen to ignore everything except the shotfile class itself


__type__ = {numpy.int32: 1, numpy.float32: 2, numpy.float64: 3,
            numpy.complex: 4, numpy.bool: 5,
            numpy.byte: 6, numpy.int64: 10, numpy.int16: 11, numpy.uint16: 12,
            numpy.uint32: 13,
            numpy.uint64: 14, numpy.character: 6, numpy.dtype('S8'): 6,
            numpy.dtype('S16'): 6,
            numpy.dtype('S32'): 6, numpy.dtype('S64'): 6}

__fields__ = {'version': lambda: numpy.int32(0),
              'level': lambda: numpy.int32(0), 'status': lambda: numpy.int32(0),
              'error': lambda: numpy.int32(0),
              'relations': lambda: numpy.zeros(8, dtype=numpy.int32),
              'address': lambda: numpy.int32(0),
              'length': lambda: numpy.int32(0), 'objnr': lambda: numpy.int32(0),
              'format': lambda: numpy.zeros(3, dtype=numpy.int32),
              'dataformat': lambda: numpy.int32(0),
              'objtype': lambda: numpy.int32(0), 'text': lambda: 64 * b' ',
              'size': lambda: numpy.int32(0),
              'indices': lambda: numpy.zeros(3, dtype=numpy.int32),
              'items': lambda: numpy.int32(0)}

__dataformat__ = {1: numpy.uint8,
                  2: numpy.character,
                  3: numpy.int16,
                  4: numpy.int32,
                  5: numpy.float32,
                  6: numpy.float64,
                  7: numpy.bool,
                  9: numpy.uint16,
                  13: numpy.int64,
                  14: numpy.uint32,
                  15: numpy.uint64,
                  1794: numpy.dtype('S8'),
                  3842: numpy.dtype('S16'),
                  7938: numpy.dtype('S32'),
                  16130: numpy.dtype('S64'),
                  18178: numpy.dtype('S72')}

__bufferlength__ = {numpy.int32: 1, numpy.float32: 1, numpy.float64: 1,
                    numpy.complex: 1, numpy.bool: 1,
                    numpy.byte: 1, numpy.int64: 1, numpy.int16: 1,
                    numpy.uint16: 1, numpy.uint32: 1,
                    numpy.uint64: 1, numpy.dtype('S8'): 8,
                    numpy.dtype('S16'): 16, numpy.dtype('S32'): 32,
                    numpy.dtype('S64'): 64, numpy.character: 1}

__obj__ = {1: 'Diagnostic', 2: 'List', 3: 'Device', 4: 'Param_Set',
           5: 'Map_Func', 6: 'Sig_Group', 7: 'Signal', 8: 'Time_Base',
           9: 'SF_List', 10: 'Algorithm', 11: 'Update_Set', 12: 'Loc_Timer',
           13: 'Area_Base', 14: 'Qualifier', 15: 'ModObj', 16: 'Map_Extd',
           17: 'Resource'}

__fmt2type__ = {2: 6, 3: 11, 4: 1, 5: 2, 6: 3, 7: 5,
                9: 12, 13: 10, 14: 13, 15: 14,
                1794: 6, 3842: 6, 7938: 6, 12034: 6, 16130: 6, 18178: 6}

__fmt2ct__ = {2: ctypes.c_char, 3: ctypes.c_int16, 4: ctypes.c_int32,
              5: ctypes.c_float, 6: ctypes.c_double, 9: ctypes.c_uint16,
              13: ctypes.c_int64, 14: ctypes.c_uint32, 15: ctypes.c_uint64}


# Define custom exceptions to allow proper error handling
class PyddError(Exception):
    '''Generic exception of py-dd wrapper,
    catch with "try ... except dd.PyddError:".'''
    # called by "PyddError('arbitrary string')"

    def print_trace(self):
        try:
            trace = self[1]['trace']
            print(trace[0])
            for el in trace[1:]:
                print('  -> %r' % el)
        except KeyError:
            print('No trace available.')

    def print_function(self):
        try:
            print(self[1]['function'])
        except KeyError:
            print('No function available')

    def print_message(self):
        print(self[0])

    def print_information(self):
        try:
            for key in self[1].keys():
                print('%s: %r' % (key, self[1][key]))
        except Exception as exc:
            print(exc)


class NoShotfileError(PyddError):
    '''Exception to be raised when trying to access data with no
    shotfile opened.'''
    # catch with "except dd.PyddError" and "except NoShotfileError"


def getError(error, **kwargs):
    """ Check if an error/warning occured. """
    isError = libddww.xxsev(error) == 1
    isWarning = libddww.xxwarn(error) == 1
    kwargs['function'] = _get_trace(stack())[
        -2] if 'function' not in kwargs else kwargs['function']
    kwargs['trace'] = _get_trace(stack())[:-1] if 'trace' not in kwargs else \
    kwargs['trace']
    if isError or isWarning:
        text = ' ' * 256
        libddww.xxerrprt(-1, text, error, 3, '')
        if isError:
            if error == 553713686:
                raise NoShotfileError(text.replace('\x00', '').strip(), kwargs)
            else:
                raise PyddError(
                    'libddww error: ' + text.replace('\x00', '').strip(),
                    kwargs)
        else:
            warnings.warn(text.replace('\x00', '').strip(), RuntimeWarning)


def _get_function_name(frame):
    try:
        return frame.f_locals['self'].__class__.__name__ + '.' \
               + frame.f_code.co_name
    except Exception:
        return frame.f_code.co_name


def _get_trace(stack_info):
    return [_get_function_name(el[0]) for el in stack_info[::-1]]


def wait(timeout=0):
    """ Wait for next AUG discharge. Default timeout is 86400s (i.e. 1 day)."""
    error = c_int32(0)
    pulseNumber = c_uint32(0)
    if timeout == 0:
        libddww.ddwait(error, pulseNumber)
    else:
        libddww.ddwaitt(error, pulseNumber, timeout)
    getError(error, function='dd.wait', timeout=timeout)
    return pulseNumber.value


def getLastAUGShotNumber():
    """ Returns the current shotnumber of ASDEX Upgrade """
    error = c_int32(0)
    pulseNumber = c_uint32(0)
    libddww.ddlastshotnr(error, pulseNumber)
    getError(error, function='dd.getLastAUGShotNumber')
    return pulseNumber.value


def getLastShotNumber(diagnostic, pulseNumber=None, experiment='AUGD'):
    """ Returns the highest available shotnumber of the specified diagnostic.
If pulseNumber is specified, the search starts from there. """
    if pulseNumber is None:
        pulseNumber = getLastAUGShotNumber()
    if pulseNumber <= 0:
        raise Exception('Invalid Shotnumber')
    cshot = c_uint32(0)
    error = libddww.ddcshotnr(experiment, diagnostic, pulseNumber, cshot)
    getError(error, shotnumber=pulseNumber, experiment=experiment,
             diagnostic=diagnostic)
    return cshot.value


def getPhysicalDimension(unit):
    """ Returns human readable physical dimension of Unit. """
    error = c_int32(0)
    output = b' ' * 256
    libddww.dddim(error, unit, output)
    try:
        getError(error, function='dd.getPhysicalDimension', Unit=unit)
        return output.replace(b'\x00', b'').strip().decode()
    except Exception:
        return ''


class dd_info(object):
    """ Class holding info objects. """

    def __init__(self):
        object.__init__(self)
        self.status = False


class objectHeader(object):
    def __init__(self, name, data, text, relationNames=None):
        object.__init__(self)
        self.name = name
        self.text = text
        self.data = data
        self.relationNames = relationNames

    @property
    def buffer(self):
        # warnings.warn('buffer will be removed in the future, please use data.', DeprecationWarning)
        return self.data

    @property
    def objectType(self):
        return __obj__[self.data[0]]

    @property
    def level(self):
        return self.data[1]

    @property
    def status(self):
        return self.data[2]

    @property
    def error(self):
        return self.data[3]

    @property
    def relations(self):
        return self.data[4:4 + 8][self.data[4:4 + 8] != 65535]

    @property
    def address(self):
        return hex(self.data[12])

    @property
    def length(self):
        return self.data[13]

    @property
    def dataFormat(self):
        return __dataformat__[self.data[14]]

    @property
    def nBytes(self):
        return self.data[13]


class signalHeader(objectHeader):
    def __init__(self, name, data, text, relationsNames=None):
        objectHeader.__init__(self, name, data, text, relationsNames)

    @property
    def unit(self):
        return getPhysicalDimension(self.data[15])


class signalGroupHeader(objectHeader):
    def __init__(self, name, data, text, relationNames=None):
        objectHeader.__init__(self, name, data, text, relationNames)

    @property
    def unit(self):
        return getPhysicalDimension(self.data[15])

    @property
    def indices(self):
        return self.data[18:22][self.data[18:22] > 1][::-1]


class areaBaseHeader(objectHeader):
    def __init__(self, name, data, text, relationNames=None):
        objectHeader.__init__(self, name, data, text, relationNames)

    @property
    def sizes(self):
        return self.data[18:21][self.data[18:21] > 1]

    @property
    def units(self):
        output = []
        for i in range(3):
            if self.data[18 + i] > 1:
                output.append(getPhysicalDimension(self.data[15 + i]))
            else:
                return output
        return output

    @property
    def nSteps(self):
        return self.data[21]


class qualifierHeader(objectHeader):
    def __init__(self, name, data, text, relationNames=None):
        objectHeader.__init__(self, name, data, text, relationNames)


class timeBaseHeader(objectHeader):
    def __init__(self, name, data, text, relationNames=None):
        objectHeader.__init__(self, name, data, text, relationNames)

    @property
    def nSteps(self):
        return self.data[21]


class parameterSetHeader(objectHeader):
    def __init__(self, name, data, text, relationNames=None):
        objectHeader.__init__(self, name, data, text, relationNames)

    @property
    def items(self):
        return self.data[15]


__headers__ = {4: parameterSetHeader,
               6: signalGroupHeader,
               7: signalHeader,
               8: timeBaseHeader,
               13: areaBaseHeader,
               14: qualifierHeader}


class signalInfo(object):
    """ Class storing the general information of a Signal. """

    def __init__(self, name, sigtype, index, timeBase):
        """ Constructor initializing the signalInfo object. """
        object.__init__(self)
        self.name = name
        self.type = sigtype
        self.index = index
        self.timeBase = timeBase

    @property
    def ndim(self):
        """ Number of dimension of the signal."""
        # corrects error in signal-groups which have 1 in the first dimension
        # this exists for the plasma current in the equilbrium files IpiPSI
        # This does not correct for 3D arrays with 1 in dimension in the other
        # locations
        return numpy.size(filter(lambda i: i > 1, self.index[1:])) + 1

    @property
    def size(self):
        """ Number of total datapoints in the signal. """
        return self.index[:self.ndim].prod()


class timeBaseInfo(object):
    """ Class storing the general information for a timebase. """

    def __init__(self, name, ntVal, nPreTrig, tBegin, tEnd):
        """ Constructor initializing the timeBaseInfo object. """
        object.__init__(self)
        self.name = name
        self.ntVal = ntVal
        self.nPreTrig = nPreTrig
        self.tBegin = tBegin
        self.tEnd = tEnd


class parameterSetInfo(object):
    def __init__(self, setName, names, items, formatSpec, devsig):
        object.__init__(self)
        self.setName = setName
        self.names = names
        self.items = items
        self.format = formatSpec
        self.devsig = devsig

    def __getitem__(self, i):
        return parameterInfo(self.setName, self.names[i], self.items[i],
                             self.format[i])


class parameterInfo(object):
    def __init__(self, setName, parName, items, formatSpec):
        object.__init__(self)
        self.setName = setName
        self.parName = parName
        self.items = items
        self.format = formatSpec


class qualifierInfo(object):
    def __init__(self, name, exists, indices, maxSection):
        object.__init__(self)
        self.name = name
        self.exists = exists
        self.indices = indices
        self.maxSection = maxSection

    @property
    def ndim(self):
        return self.indices[self.indices > 1].size

    @property
    def size(self):
        return self.indices[self.indices > 1].prod()


class qualifier(object):
    def __init__(self, name, status, data):
        object.__init__(self)
        self.name = name
        self.status = status
        self.data = data


class mappingInfo(object):
    def __init__(self, name, device, channel):
        object.__init__(self)
        self.name = name
        self.device = device
        self.channel = channel


class parameter(object):
    def __init__(self, setName, parName, data, unit):
        object.__init__(self)
        self.setName = setName
        self.name = parName
        self.data = data
        self.unit = unit


class parameterSet(dict):
    def __init__(self, setName):
        dict.__init__(self)
        self.name = setName


class signal(object):
    def __init__(self, name, header, data, time=None, unit='', area=None):
        object.__init__(self)
        self.name = name
        self.header = header
        self.data = data
        self.time = time
        self.unit = unit
        self.area = area

    @property
    def description(self):
        return self.header.text

    def __call__(self, tBegin, tEnd):
        if self.time is None:
            raise PyddError('Signal is not time dependent.')
        if tBegin == tEnd:
            index = numpy.argmin(numpy.abs(self.time - tBegin))
        else:
            index = numpy.arange(self.time.size)[
                (self.time >= tBegin) * (self.time <= tEnd)]
        return signal(self.name, self.header, self.data[index],
                      self.time[index], self.unit)

    def max(self):
        try:
            return numpy.nanmax(self.data)
        except Exception:
            return numpy.nan

    def min(self):
        try:
            return numpy.nanmin(self.data)
        except Exception:
            return numpy.nan

    def median(self):
        try:
            return numpy.median(self.data)
        except Exception:
            return numpy.nan

    def mean(self):
        try:
            return numpy.mean(self.data)
        except Exception:
            return numpy.nan

    def std(self):
        try:
            return numpy.std(self.data)
        except Exception:
            return numpy.nan

    @property
    def size(self):
        return self.data.size

    def removeNaN(self):
        index = self.data == self.data
        self.data = self.data[index]
        if self.time is not None:
            self.time = self.time[index]

    def removeInvalid(self):
        self.removeNaN()
        index = numpy.isfinite(self.data)
        self.time = self.time[index]
        self.data = self.data[index]

    def __iadd__(self, rhs):
        try:
            self.data += numpy.interp(self.time, rhs.time, rhs.data)
            self.name = '(%s + %s)' % (self.name, rhs.name)
        except AttributeError:
            self.data += rhs
        return self

    def __add__(self, rhs):
        try:
            return signal('(%s + %s)' % (self.name, rhs.name), self.header,
                          self.data + numpy.interp(self.time, rhs.time,
                                                   rhs.data), self.time)
        except AttributeError:
            return signal(self.name, self.header, self.data + rhs, self.time)

    def __radd__(self, rhs):
        return self.__add__(rhs)

    def __isub__(self, rhs):
        try:
            self.data -= numpy.interp(self.time, rhs.time, rhs.data)
            self.name = '(%s - %s)' % (self.name, rhs.name)
        except AttributeError:
            self.data -= rhs
        return self

    def __sub__(self, rhs):
        try:
            return signal('(%s - %s)' % (self.name, rhs.name), self.header,
                          self.data - numpy.interp(self.time, rhs.time,
                                                   rhs.data), self.time)
        except AttributeError:
            return signal(self.name, self.header, self.data - rhs, self.time)

    def __rsub__(self, rhs):
        return signal(self.name, self.header, rhs - self.data, self.time)

    def __pow__(self, exponent):
        return signal(self.name, self.header, self.data ** exponent, self.time)

    def __imul__(self, rhs):
        try:
            self.data *= numpy.interp(self.time, rhs.time, rhs.data)
            self.name = '(%s * %s)' % (self.name, rhs.name)
        except AttributeError:
            self.data *= rhs
        return self

    def __mul__(self, rhs):
        try:
            return signal('(%s * %s)' % (self.name, rhs.name), self.header,
                          self.data * numpy.interp(self.time, rhs.time,
                                                   rhs.data), self.time)
        except AttributeError:
            return signal(self.name, self.header, self.data * rhs, self.time)

    def __rmul__(self, rhs):
        return self.__mul__(rhs)

    def __idiv__(self, rhs):
        try:
            self.data /= numpy.interp(self.time, rhs.time, rhs.data)
            self.name = '(%s / %s)' % (self.name, rhs.name)
        except AttributeError:
            self.data /= rhs
        return self

    def __div__(self, rhs):
        try:
            return signal('%s / %s' % (self.name, rhs.name), self.header,
                          self.data / numpy.interp(self.time, rhs.time,
                                                   rhs.data), self.time)
        except AttributeError:
            return signal(self.name, self.header, self.data / rhs, self.time)

    def __rdiv__(self, rhs):
        return signal(self.name, self.header, rhs / self.data, self.time)

    def interp(self, time):
        return numpy.interp(time, self.time, self.data)


class signalGroup(object):
    def __init__(self, name, header, data, time=None, unit='', area=None):
        object.__init__(self)
        self.name = name
        self.header = header
        self.data = data
        self.time = time
        self.unit = unit
        self.area = area

    @property
    def description(self):
        return self.header.text

    def __call__(self, tBegin, tEnd):
        if self.time is None:
            raise PyddError('SignalGroup is not time dependent.')
        if tBegin == tEnd:
            index = numpy.argmin(numpy.abs(self.time - tBegin))
        else:
            index = numpy.arange(self.time.size)[
                (self.time >= tBegin) * (self.time <= tEnd)]
        area = copy(self.area)
        area.data = area.data[index]
        return signal(self.name, self.header, self.data[index],
                      self.time[index], self.unit, area)

    def __getitem__(self, indices):
        if self.time is None:
            if self.data.ndim == numpy.size(indices):
                return signal(self.name, self.header, self.data[indices], None,
                              self.unit)
            else:
                raise TypeError(
                    'Wrong number of indices provided. Get %d needed %d.'
                    % (numpy.size(indices), self.data.ndim)
                )
        else:
            if self.data.ndim - 1 == numpy.size(indices):
                if numpy.size(indices) == 1:
                    try:
                        return signal(self.name, self.header,
                                      self.data[:, indices[0]], self.time,
                                      self.unit)
                    except Exception:
                        return signal(self.name, self.header,
                                      self.data[:, indices], self.time,
                                      self.unit)
                elif numpy.size(indices) == 2:
                    return signal(self.name, self.header,
                                  self.data[:, indices[0], indices[1]],
                                  self.time, self.unit)
                elif numpy.size(indices) == 3:
                    return signal(self.name,
                                  self.header,
                                  self.data[:, indices[0], indices[1],
                                    indices[2]],
                                  self.time, self.unit)
                else:
                    raise TypeError(
                        'Invalid number of indices: Got %d needed %d'
                        % (numpy.size(indices), (self.data.nnim - 1))
                    )

    def max(self, axis=None):
        return numpy.nanmax(self.data, axis=axis)

    def min(self, axis=None):
        return numpy.nanmin(self.data, axis=axis)

    def median(self, axis=None):
        return numpy.median(self.data, axis=axis)

    def mean(self, axis=None):
        return numpy.mean(self.data, axis=axis)

    @property
    def shape(self):
        return self.data.shape

    @property
    def size(self):
        return self.data.size

    @property
    def ndim(self):
        return self.data.ndim


class areaBaseInfo(areaBaseHeader):
    def __init__(self, name, data, text, relationNames, timeIndex):
        areaBaseHeader.__init__(self, name, data, text, relationNames)

        self.timeIndex = timeIndex
        if timeIndex == 2:
            self.data[20], self.data[21] = self.data[21], self.data[20]
        elif timeIndex == 3:
            self.data[19], self.data[21] = self.data[21], self.data[19]


class areaBase(object):
    def __init__(self, name, header, x, y=None, z=None):
        object.__init__(self)
        self.name = name
        self.header = header
        self.x = x
        self.y = y
        self.z = z

    @property
    def shape(self):
        return (self.x.size, 0 if self.y is None else self.y.size,
                0 if self.z is None else self.z.size)

    # def __getitem__(self, i):
    #    return self.data[i]

    @property
    def units(self):
        return self.header.units if len(self.header.units) != 1 else \
        self.header.units[0]


## \endcond
# see top

class shotfile(object):
    """ Class to load the data from the shotfile. """

    def __init__(self, diagnostic=None, pulseNumber=None, experiment='AUGD',
                 edition=0, diagnostic2=None):
        """ Basic constructor. If a diagnostic is specified the shotfile is opened.
        Example: sf = dd.shotfile('MSX', 29761) """
        self.diaref = ctypes.c_int32(0)
        self.edition = None
        self.shot = None
        if diagnostic is not None and pulseNumber is not None:
            self.open(diagnostic, pulseNumber, experiment, edition, diagnostic2)

    def __del__(self):
        """ Basic destructor closing the shotfile. """
        self.close()

    @property
    def status(self):
        """ Status showing if shotfile is open."""
        return self.diaref.value != 0

    def _get_error(self, error, **kwargs):
        if self.status:
            kwargs['shotnumber'] = self.shot
            kwargs['edition'] = self.edition
            kwargs['experiment'] = self.experiment
            kwargs['diagnostic'] = self.diagnostic
            kwargs['function'] = _get_trace(stack())[
                -2] if 'function' not in kwargs else kwargs['function']
            kwargs['trace'] = _get_trace(stack())[:-1] \
                if 'trace' not in kwargs else kwargs['trace']
            getError(error, **kwargs)

    def open(self, diagnostic, pulseNumber, experiment='AUGD', edition=0,
             diagnostic2=None):
        """ Function opening the specified shotfile. """
        self.close()
        error = c_int32(0)
        date = b' ' * 18
        libddww.ddopen(error, experiment, diagnostic, pulseNumber, edition,
                       self.diaref, date)
        getError(error, experiment=experiment, diagnostic=diagnostic,
                 pulseNumber=pulseNumber, edition=edition)
        self.shot = pulseNumber
        # workaround because ddopen() does not return edition (nor shot, for that matter):
        self.experiment = experiment
        self.diagnostic = diagnostic
        self.date = date.decode().strip()
        self.edition = self.getObjectHeader(
            diagnostic if diagnostic2 is None else diagnostic2).data[3]

    def close(self):
        """ Close the shotfile. """
        if self.status:
            error = c_int32(0)
            libddww.ddclose(error, self.diaref)
            self._get_error(error)
            self.diaref.value = 0
            self.edition = None
            self.shot = None
            del self.date

    def getObjectName(self, objectNumber):
        """ Return name of object """
        if not self.status:
            raise PyddError('Shotfile not open!')
        name = b' ' * 8
        error = c_int32(0)
        libddww.ddobjname(error, self.diaref, objectNumber, name)
        self._get_error(error, object_number=objectNumber)
        return name.replace(b'\x00', b'').strip().decode()

    def getObjectNames(self, needle=None):
        """ Return list of all object names in the shotfile.
        Optional: only names that contain 'needle'. """
        if not self.status:
            raise PyddError('Shotfile not open!')
        output = {}
        counter = 0
        while True:
            try:
                name = self.getObjectName(counter)
                counter += 1
                if needle is not None and needle not in name:
                    continue
                output[counter - 1] = name.encode()
            except Exception:
                return output

    def getShotfileCreationDate(self, formatstr='%d%b%Y;%H:%M:%S'):
        """ Returns the date of creation of the shotfile as a datetime.datetime
        object there are several different formats depending on the diagnostic
        standard is e.g. '13May2014;14:33:41'
        """
        if not self.status:
            raise Exception('Shotfile not open!')
        return datetime.datetime.strptime(self.date, formatstr)

    def getSignalNames(self):
        """ Return list of all signal names in the shotfile. """
        if not self.status:
            raise PyddError('Shotfile not open!')
        output = []
        names = self.getObjectNames()
        for key in names:
            name = names[key]
            if self.getObjectValue(name, 'objtype') == 7:
                output.append(name)
        return output

    def getSignalGroupNames(self):
        """ Return list of all signalgroup names in the shotfile. """
        if not self.status:
            raise PyddError('Shotfile not open!')
        output = []
        names = self.getObjectNames()
        for key in names:
            name = names[key]
            if self.getObjectValue(name, 'objtype') == 6:
                output.append(name)
        return output

    def getSignalInfo(self, name):
        """ Returns a signalInfo object containing the information of the signal name """
        if not self.status:
            raise PyddError('Shotfile not open!')
        header = self.getObjectHeader(name)
        error = c_int32(0)
        typ = c_uint32(0)
        tname = b' ' * 8
        ind = numpy.zeros(4, dtype=numpy.uint32)
        libddww.ddsinfo(error, self.diaref, name, typ, tname, ind)
        self._get_error(error, name=name)
        if header.objectType == 'Time_Base':
            tname = name.encode()

        return signalInfo(name.encode().replace(b'\x00', b'').strip().decode(), typ.value, ind,
                          tname.replace(b'\x00', b'').strip().decode())

    def getObjectValue(self, name, field):
        """ Returns the value for the specified field for the given object name. """
        if not self.status:
            raise PyddError('Shotfile not open')
        error = c_int32(0)
        if field == 'text':
            output = ' ' * 64
            libddww.ddobjval(error, self.diaref, name, field, output)
            output = output.replace('\x00', '').strip()
        elif field in ['indices', 'format']:
            output = numpy.zeros(3, dtype=numpy.int32)
            libddww.ddobjval(error, self.diaref, name, field,
                             output.ctypes.data_as(c_void_p))
        elif field == 'relations':
            output = numpy.zeros(8, dtype=numpy.int32)
            libddww.ddobjval(error, self.diaref, name, field,
                             output.ctypes.data_as(c_void_p))
        else:
            value = c_int32(0)
            libddww.ddobjval(error, self.diaref, name, field,
                             ctypes.byref(value))
            output = value.value
        self._get_error(error, name=name, field=field)
        return output

    def __call__(self, name, dtype=None, tBegin=None, tEnd=None,
                 calibrated=True, index=None):
        """ Unified function to read data from a signal, signalgroup or timebase.
        Keywords:
        dtype: If desired the datatype of the output can be specified. If no dtype is specified,
        the data will be returned as float32 in case of calibrated data and in the format
        used in the shotfile in case of uncalibrated data.
        tBegin: Sets the starting point from which time data will be read. Note here that for signals
        without a time dependence or in case of signal groups where the time index is not the
        first index this keyword has no effect.
        tEnd: Similar to tBegin but this sets the time until which the data will be read.
        calibrated: If True calibrated data together with the unit will be returned. If False the data
        as written in the shotfile will be returned.

        Example:
        tot = dd.shotfile('TOT', 29761)
        Bt = tot('BTF')
        plt.plot(Bt.time, Bt.data)
        plt.show() """

        if not self.status:
            raise PyddError('Shotfile not open!')
        objectType = self.getObjectValue(name, 'objtype')
        if objectType == 4:
            return self.getParameterSet(name, dtype=dtype)
        elif objectType == 5:
            raise PyddError('Mapping function not yet implemented.')
        elif objectType == 6:
            if calibrated:
                if index is None:
                    data, unit = self.getSignalGroupCalibrated(name,
                                                               dtype=dtype,
                                                               tBegin=tBegin,
                                                               tEnd=tEnd)
                else:
                    data, unit = self.getSignalGroupSliceCalibrated(name, index,
                                                                    dtype=dtype,
                                                                    tBegin=tBegin,
                                                                    tEnd=tEnd)
            else:
                if index is None:
                    data = self.getSignalGroup(name, dtype=dtype, tBegin=tBegin,
                                               tEnd=tEnd)
                    unit = ''
                else:
                    data = self.getSignalGroupSlice(name, index, dtype=dtype,
                                                    tBegin=tBegin, tEnd=tEnd)
                    unit = ''
            try:
                time = self.getTimeBase(name, tBegin=tBegin, tEnd=tEnd)
            except Exception:
                time = None
            return signalGroup(name, self.getObjectHeader(name), data, time,
                               unit, area=self.getAreaBase(name))
        elif objectType == 7:
            if calibrated:
                if dtype not in [numpy.float32, numpy.float64]:
                    dtype = numpy.float32
                data, unit = self.getSignalCalibrated(name, dtype=dtype,
                                                      tBegin=tBegin, tEnd=tEnd)
            else:
                data = self.getSignal(name, dtype=dtype, tBegin=tBegin,
                                      tEnd=tEnd)
                unit = ''
            try:
                time = self.getTimeBase(name, tBegin=tBegin, tEnd=tEnd)
            except Exception:
                time = None
            return signal(name, self.getObjectHeader(name), data, time=time,
                          unit=unit, area=self.getAreaBase(name))
        elif objectType == 8:
            if dtype not in [numpy.float32, numpy.float64]:
                dtype = numpy.float32
            return self.getTimeBase(name, dtype=dtype, tBegin=tBegin, tEnd=tEnd)
        elif objectType == 13:
            return self.getAreaBase(name)
        else:
            raise PyddError(
                'Unsupported object type %d for object %s' % (objectType, name))

    def getSignal(self, name, dtype=None, tBegin=None, tEnd=None):
        """ Return uncalibrated signal. If dtype is specified the data is
        converted accordingly, else the data is returned in the format used
        in the shotfile. """
        if not self.status:
            raise PyddError('Shotfile not open!')
        info = self.getSignalInfo(name)
        try:
            tInfo = self.getTimeBaseInfo(name)
            if tBegin is None:
                tBegin = tInfo.tBegin
            if tEnd is None:
                tEnd = tInfo.tEnd
            if tInfo.ntVal == info.index[0]:
                k1, k2 = self.getTimeBaseIndices(name, tBegin, tEnd)
            else:
                k1 = 1
                k2 = info.index[0]
        except Exception:
            k1 = 1
            k2 = info.index[0]
        try:
            typ = c_uint32(__type__[dtype])
            data = numpy.zeros(k2 - k1 + 1, dtype=dtype)
        except KeyError:
            dataformat = self.getObjectValue(name, 'dataformat')
            typ = ctypes.c_uint32(0)
            data = numpy.zeros(k2 - k1 + 1, dtype=__dataformat__[dataformat])
        error = c_int32(0)
        leng = c_uint32(0)
        lbuf = c_uint32(k2 - k1 + 1)
        libddww.ddsignal(error, self.diaref, name, k1, k2, typ, lbuf, data,
                         leng)
        self._get_error(error, name=name, dtype=dtype, tBegin=tBegin, tEnd=tEnd)
        return data

    def getSignalCalibrated(self, name, dtype=numpy.float32, tBegin=None,
                            tEnd=None):
        """ Return calibrated signal. If dtype is specified the data is
        converted accordingly, else the data is returned as numpy.float32. """
        if not self.status:
            raise PyddError('Shotfile not open!')
        info = self.getSignalInfo(name)
        try:
            tInfo = self.getTimeBaseInfo(name)
            if tBegin is None:
                tBegin = tInfo.tBegin
            if tEnd is None:
                tEnd = tInfo.tEnd
            if tInfo.ntVal == info.index[0]:
                k1, k2 = self.getTimeBaseIndices(name, tBegin, tEnd)
            else:
                k1 = 1
                k2 = info.index[0]
        except Exception:
            k1 = 1
            k2 = info.index[0]
        if dtype not in [numpy.float32, numpy.float64]:
            dtype = numpy.float32
        typ = c_uint32(__type__[dtype])
        data = numpy.zeros(k2 - k1 + 1, dtype=dtype)
        error = c_int32(0)
        leng = c_uint32(0)
        lbuf = c_uint32(k2 - k1 + 1)
        ncal = c_int32(0)
        physdim = b' ' * 8
        libddww.ddcsgnl(error, self.diaref, name, k1, k2,
                        typ, lbuf, data, leng,
                        ncal, physdim)
        self._get_error(error, name=name, dtype=dtype, tBegin=tBegin, tEnd=tEnd)
        return data, physdim.replace(b'\x00', b'').strip().decode()

    def getSignalGroup(self, name, dtype=None, tBegin=None, tEnd=None):
        """ Return uncalibrated signalgroup. If dtype is specified the data is
        converted accordingly, else the data is returned in the format used
        in the shotfile. """
        if not self.status:
            raise PyddError('Shotfile not open!')
        info = self.getSignalInfo(name)
        try:
            tInfo = self.getTimeBaseInfo(name)
            tSet = False if tBegin is None and tEnd is None else True
            if tBegin is None:
                tBegin = tInfo.tBegin
            if tEnd is None:
                tEnd = tInfo.tEnd
            if info.index[0] == tInfo.ntVal:
                k1, k2 = self.getTimeBaseIndices(name, tBegin, tEnd)
            else:
                if tSet:
                    warnings.warn('Length of time base & 1st index of signal '
                                  'group "%s" not matching. Ignoring '
                                  'tBegin/tEnd as a precaution.' % name,
                                  RuntimeWarning)
                k1 = 1
                k2 = info.index[0]
        except Exception:
            k1 = 1
            k2 = info.index[0]
        index = numpy.append(k2 - k1 + 1, info.index[1:])
        try:
            typ = __type__[dtype]
            data = numpy.zeros(index[:info.ndim], dtype=dtype, order='F')
        except KeyError:
            dataformat = self.getObjectValue(name, 'dataformat')
            typ = 0
            data = numpy.zeros(index[:info.ndim],
                               dtype=__dataformat__[dataformat], order='F')
        error = c_int32(0)
        leng = c_uint32(0)
        lbuf = k2 - k1 + 1
        libddww.ddsgroup(error, self.diaref, name, k1,
                         k2, typ, lbuf, data,
                         leng)
        self._get_error(error, name=name, dtype=dtype, tBegin=tBegin, tEnd=tEnd)
        # If it has no relations return as it is
        if len(self.getRelations(name).typ) == 0:
            return data
        # transpose data if timebase not first index, e.g. IDA
        return data if self.getRelations(name).typ[0] == 8 else data.T

    def getSignalGroupSlice(self, name, idx, dtype=None, tBegin=None,
                            tEnd=None):
        """ Return uncalibrated signalgroup data slice. If dtype is specified the
        data is converted accordingly, else the data is returned in the format
        used in the shotfile. """
        if not self.status:
            raise PyddError('Shotfile not open!')
        info = self.getSignalInfo(name)
        try:
            tInfo = self.getTimeBaseInfo(name)
            tSet = False if tBegin is None and tEnd is None else True
            if tBegin is None:
                tBegin = tInfo.tBegin
            if tEnd is None:
                tEnd = tInfo.tEnd
            if info.index[0] == tInfo.ntVal:
                k1, k2 = self.getTimeBaseIndices(name, tBegin, tEnd)
            else:
                if tSet:
                    warnings.warn('Length of time base & 1st index of signal '
                                  'group "%s" not matching. Ignoring '
                                  'tBegin/tEnd as a precaution.' % name,
                                  RuntimeWarning)
                k1 = 1
                k2 = info.index[0]
        except Exception:
            k1 = 1
            k2 = info.index[0]
        size = (k2 - k1 + 1)
        # index = numpy.append(k2-k1+1, info.index[1:])
        try:
            typ = __type__[dtype]
            data = numpy.zeros(size, dtype=dtype, order='F')
        except KeyError:
            dataformat = self.getObjectValue(name, 'dataformat')
            typ = 0
            data = numpy.zeros(size, dtype=__dataformat__[dataformat],
                               order='F')
        error = c_int32(0)
        leng = c_uint32(0)
        lbuf = k2 - k1 + 1
        sidx = ctypes.c_uint32(idx)
        # DDXTRSIGNAL (error, diaref, name, k1, k2, indices, type,lbuf, buffer, leng)
        libddww.ddxtrsignal(error, self.diaref, name, k1, k2,
                            sidx, typ, lbuf, data,
                            leng)
        self._get_error(error, name=name, idx=idx, dtype=dtype, tBegin=tBegin,
                        tEnd=tEnd)
        # If it has no relations return as it is
        if len(self.getRelations(name).typ) == 0:
            return data
        # transpose data if timebase not first index, e.g. IDA
        return data if self.getRelations(name).typ[0] == 8 else data.T

    def getSignalGroupSliceCalibrated(self, name, idx, dtype=None, tBegin=None,
                                      tEnd=None):
        """Return calibrated signalgroup data slice (single track). If dtype is specified
        the data is converted accordingly, else the data is returned in the format used
        in the shotfile. """
        if not self.status:
            raise PyddError('Shotfile not open!')
        info = self.getSignalInfo(name)
        try:
            tInfo = self.getTimeBaseInfo(name)
            tSet = False if tBegin is None and tEnd is None else True
            if tBegin is None:
                tBegin = tInfo.tBegin
            if tEnd is None:
                tEnd = tInfo.tEnd
            if info.index[0] == tInfo.ntVal:
                k1, k2 = self.getTimeBaseIndices(name, tBegin, tEnd)
            else:
                if tSet:
                    warnings.warn('Length of time base & 1st index of signal '
                                  'group "%s" not matching. Ignoring '
                                  'tBegin/tEnd as a precaution.' % name,
                                  RuntimeWarning)
                k1 = 1
                k2 = info.index[0]
        except Exception:
            k1 = 1
            k2 = info.index[0]
        size = (k2 - k1 + 1)
        # index = numpy.append(k2-k1+1, info.index[1:])
        if dtype not in [numpy.float32, numpy.float64]:
            dtype = numpy.float32
        try:
            typ = __type__[dtype]
            data = numpy.zeros(size, dtype=dtype, order='F')
        except KeyError:
            dataformat = self.getObjectValue(name, 'dataformat')
            typ = 0
            data = numpy.zeros(size, dtype=__dataformat__[dataformat],
                               order='F')
        error = c_int32(0)
        leng = c_uint32(0)
        lbuf = k2 - k1 + 1
        physdim = b' ' * 8
        ncal = c_int32(0)
        sidx = c_uint32(idx)
        #  DDCXSIG (error, diaref, name, k1, k2, indices, type, lbuf, buffer, leng, ncal, physdim)
        libddww.ddcxsig(error, self.diaref, name, k1,
                        k2, sidx, typ, lbuf, data,
                        leng, ncal, physdim)
        self._get_error(error, name=name, idx=idx, dtype=dtype, tBegin=tBegin,
                        tEnd=tEnd)
        physim = physdim.replace(b'\x00', b'').strip()
        # If it has no relations return as it is
        if len(self.getRelations(name).typ) == 0:
            return data, physim
        # transpose data if timebase not first index, e.g. IDA
        return (data, physim) if self.getRelations(name).typ[0] == 8 \
            else (data.T, physdim)

    def getSignalGroupCalibrated(self, name, dtype=numpy.float32, tBegin=None,
                                 tEnd=None):
        if not self.status:
            raise PyddError('Shotfile not open!')
        info = self.getSignalInfo(name)
        try:
            tInfo = self.getTimeBaseInfo(name)
            tSet = False if tBegin is None and tEnd is None else True
            if tBegin is None:
                tBegin = tInfo.tBegin
            if tEnd is None:
                tEnd = tInfo.tEnd
            if info.index[0] == tInfo.ntVal:
                k1, k2 = self.getTimeBaseIndices(name, tBegin, tEnd)
            else:
                if tSet:
                    warnings.warn('Length of time base & 1st index of signal '
                                  'group "%s" not matching. Ignoring '
                                  'tBegin/tEnd as a precaution.' % name,
                                  RuntimeWarning)
                k1 = 1
                k2 = info.index[0]
        except Exception:
            k1 = 1
            k2 = info.index[0]

        index = numpy.append(k2 - k1 + 1, info.index[1:])
        if dtype not in [numpy.float32, numpy.float64]:
            dtype = numpy.float32
        typ = __type__[dtype]
        data = numpy.zeros(index[:info.ndim], dtype=dtype, order='F')
        error = c_int32(0)
        leng = c_uint32(0)
        lbuf = k2 - k1 + 1
        ncal = c_int32(0)
        physdim = ' ' * 8
        libddww.ddcsgrp(error, self.diaref, name, k1, k2,
                        typ, lbuf, data, leng,
                        ncal, physdim)
        self._get_error(error, name=name, dtype=dtype, tBegin=tBegin, tEnd=tEnd)
        physdim = physdim.replace('\x00', '').strip()
        # If it has no relations return as it is
        if len(self.getRelations(name).typ) == 0:
            return data, physdim
        # transpose data if timebase not first index, e.g. IDA
        return (data, physdim) if self.getRelations(name).typ[0] == 8 \
            else (data.T, physdim)

    def getTimeBaseInfo(self, name):
        """ Return information regarding timebase corresponding to name. """
        if not self.status:
            raise PyddError('Shotfile not open!')
        info = self.getSignalInfo(name)
        error = c_int32(0)
        ntval = c_uint32(0)
        npretrig = c_uint32(0)
        time1 = c_float(0.0)
        time2 = c_float(0.0)
        libddww.ddtrange(error, self.diaref, name, time1, time2, ntval,
                         npretrig)
        self._get_error(error, name=name)
        return timeBaseInfo(info.timeBase, ntval.value, npretrig.value,
                            time1.value, time2.value)

    def getTimeBase(self, name, dtype=numpy.float32, tBegin=None, tEnd=None):
        """ Return timebase corresponding to name. """
        if not self.status:
            raise PyddError('Shotfile not open!')
        info = self.getSignalInfo(name)
        tInfo = self.getTimeBaseInfo(name)
        if info.timeBase != name:
            return self.getTimeBase(info.timeBase, dtype=dtype, tBegin=tBegin,
                                    tEnd=tEnd)
        if tBegin is None:
            tBegin = tInfo.tBegin
        if tEnd is None:
            tEnd = tInfo.tEnd
        typ = __type__[dtype]
        error = c_int32(0)
        if info.index[0] == tInfo.ntVal:
            k1, k2 = self.getTimeBaseIndices(name, tBegin, tEnd)
        else:
            k1 = 1
            k2 = info.index[0]
        if dtype not in [numpy.float32, numpy.float64]:
            dtype = numpy.float32
        data = numpy.zeros(k2 - k1 + 1, dtype=dtype)
        length = c_uint32(0)
        libddww.ddtbase(error, self.diaref, name, k1, k2, typ, data.size, data,
                        length)
        self._get_error(error, name=name, dtype=dtype, tBegin=tBegin, tEnd=tEnd)
        return data

    def getTimeBaseIndices(self, name, tBegin, tEnd):
        """ Return time indices of name corresponding to tBegin and tEnd """
        if not self.status:
            raise PyddError('Shotfile not open!')
        error = c_int32(0)
        info = self.getTimeBaseInfo(name)
        if tEnd < tBegin:
            tBegin, tEnd = tEnd, tBegin
        if tBegin < info.tBegin:
            tBegin = info.tBegin
        if tEnd > info.tEnd:
            tEnd = info.tEnd
        k1 = c_uint32(0)
        k2 = c_uint32(0)
        libddww.ddtindex(error, self.diaref, name, tBegin, tEnd, k1, k2)
        self._get_error(error, name=name, tBegin=tBegin, tEnd=tEnd)
        return k1.value, k2.value

    def getParameterSetInfo(self, name):
        if not self.status:
            raise PyddError('Shotfile not open!')
        error = c_int32(0)
        info = self.getObjectValue(name, 'items')
        nrec = c_int32(info)
        rname = ' ' * 9 * info
        items = numpy.zeros(info, dtype=numpy.uint32)
        formatSpec = numpy.zeros(info, dtype=numpy.uint16)
        devsig = numpy.zeros(info, dtype=numpy.int32)
        libddww.ddprinfo(error, self.diaref, name, nrec,
                         rname, items,
                         formatSpec, devsig)
        self._get_error(error, name=name)
        rname = rname.replace(' ',
                              '\x00')  # corrects issue with libddww dsprinfo
        # on Solaris machines where spaces are used instead of \x00 (this differs
        # from the toki linux servers
        names = [el for el in rname.split('\x00') if el != '']
        return parameterSetInfo(name, names, items, formatSpec, devsig)

    def getParameterInfo(self, setName, parName):
        if not self.status:
            raise PyddError('Shotfile not open!')
        error = c_int32(0)
        item = c_uint32(0)
        formatSpec = c_uint16(0)
        libddww.dd_prinfo(error, self.diaref, setName, parName, item, formatSpec)
        self._get_error(error, setName=setName, parName=parName)
        return parameterInfo(setName, parName, item.value, formatSpec.value)

    def getParameter(self, setName, parName, dtype=None):
        if not self.status:
            raise PyddError('Shotfile not open!')
        info = self.getParameterInfo(setName, parName)
        error = c_int32(0)
        try:
            typ = __type__[dtype]
            data = numpy.zeros(info.items, dtype=dtype)
        except KeyError:
            dtype = __dataformat__[info.format]
            typ = __type__[dtype]
            data = numpy.zeros(info.items, dtype=dtype)
        lbuf = c_uint32(info.items * __bufferlength__[dtype])
        physunit = c_int32(0)
        libddww.ddparm(error, self.diaref, setName, parName, typ, lbuf, data,
                       physunit)
        self._get_error(error, setName=setName, parName=parName, dtype=dtype)
        if data.size == 1:
            return parameter(setName, parName, data[0],
                             getPhysicalDimension(physunit.value))

        return parameter(setName, parName, data,
                         getPhysicalDimension(physunit.value))

    def getParameterSet(self, setName, dtype=None):
        if not self.status:
            raise PyddError('Shotfile not open!')
        info = self.getParameterSetInfo(setName)
        output = parameterSet(setName)
        for name in info.names:
            output[name] = self.getParameter(setName, name, dtype=dtype)
        return output

    def getList(self, name):
        if not self.status:
            raise PyddError('Shotfile not open!')
        error = c_int32(0)
        listlen = c_int32(256)
        nlist = ' ' * 256
        libddww.ddlnames(error, self.diaref, name, listlen, nlist)
        self._get_error(error, name=name)
        return [el for el in nlist.replace(' ', '').split('\x00') if el != '']

    def getMappingInfo(self, name, indices=None):
        if not self.status:
            raise PyddError('Shotfile not open!')
        if indices is None:
            indices = numpy.ones(3, dtype=numpy.uint32)
        else:
            indices = numpy.uint32(indices)
        error = c_int32(0)
        devname = ' ' * 8
        channel = c_int32(0)
        libddww.ddmapinfo(error, self.diaref, name, indices,
                          devname, channel)
        self._get_error(error, name=name, indices=None)
        return mappingInfo(name, devname.replace('\x00', ''), channel.value)

    def GetSignal(self, name, cal=False):
        warnings.warn('GetSignal will be removed in the future.',
                      DeprecationWarning)
        if not self.status:
            raise PyddError('Shotfile not open!')
        objectType = self.getObjectValue(name, 'objtype')
        if objectType == 6:
            if cal:
                return self.getSignalGroupCalibrated(name)[0]
            return self.getSignalGroup(name)
        elif objectType == 7:
            if cal:
                return self.getSignalCalibrated(name)[0]
            return self.getSignal(name)
        else:
            raise PyddError('Unsupported object type: %d' % objectType,
                            function='shotfile.GetSignal', name=name, cal=cal)

    def getAreaBaseInfo(self, name):
        if not self.status:
            raise PyddError('Shotfile not open!')
        header = self.getObjectHeader(name)
        if header.objectType == 'Area_Base':
            for el in self.getRelatingObjects(name):
                objHeader = self.getObjectHeader(el)
                if objHeader.objectType in ['Sig_Group', 'Signal']:
                    signalName = el
                    break
            return areaBaseInfo(header.name, header.data, header.text,
                                header.relationNames, -1)
        else:
            signalName = name
        error = c_int32(0)
        sizes = numpy.zeros(3, dtype=numpy.uint32)
        adim = numpy.zeros(3, dtype=numpy.uint32)
        index = c_int32(0)
        libddww.ddainfo(error, self.diaref, signalName, sizes, adim, index)
        self._get_error(error, name=name)
        return areaBaseInfo(header.name, header.data, header.text,
                            header.relationNames, index.value)

    def getAreaBase(self, name, dtype=numpy.float32, tBegin=None, tEnd=None):
        if not self.status:
            raise PyddError('Shotfile not open')
        header = self.getObjectHeader(name)
        if header.objectType == 'Area_Base':
            area_base_info = self.getAreaBaseInfo(name)
            error = c_int32(0)
            length = c_uint32(0)
            data = numpy.zeros(
                (area_base_info.nSteps, area_base_info.sizes.sum()),
                dtype=dtype)
            libddww.ddagroup(error, self.diaref, name, 1, area_base_info.nSteps,
                             __type__[dtype], data.size / area_base_info.nSteps,
                             data, length)
            x = data[:, :area_base_info.sizes[0]]
            if area_base_info.sizes.size >= 2:
                y = data[:, area_base_info.sizes[0]:area_base_info.sizes[0]
                                                    + area_base_info.sizes[1]]
            else:
                y = None
            if area_base_info.sizes.size >= 3:
                z = data[:, area_base_info.sizes[0] + area_base_info.sizes[1]:
                            area_base_info.sizes[0] + area_base_info.sizes[1]
                            + area_base_info.sizes[2]]
            else:
                z = None
            return areaBase(name, header, x, y, z)
        elif header.objectType in ['Sig_Group', 'Signal']:
            areaBases = []
            for el in header.relationNames:
                if self.getObjectHeader(el).objectType == 'Area_Base':
                    # There have been cases where time bases had object type
                    # Area_Base. Reason for this is so far unknown. Since it
                    # resulted in "None" being returned from getAreaBase,
                    # a check had to be introduced:
                    toAppend = self.getAreaBase(el, dtype=dtype, tBegin=tBegin,
                                                tEnd=tEnd)
                    if toAppend:
                        areaBases.append(toAppend)
            if len(areaBases) > 1:
                output = {}
                for el in areaBases:
                    output[el.name] = el
                return output
            return areaBases[0] if len(areaBases) != 0 else None

    def getQualifierInfo(self, name):
        if not self.status:
            raise PyddError('Shotfile not open!')
        error = c_int32(0)
        exist = c_int32(0)
        indices = numpy.zeros(3, dtype=numpy.uint32)
        maxsection = c_uint32(0)
        libddww.ddqinfo(error, self.diaref, name, exist, indices, maxsection)
        self._get_error(error, name=name)
        return qualifierInfo(name, exist.value, indices, maxsection.value)

    def getQualifier(self, name):
        if not self.status:
            raise PyddError('Shotfile not open!')
        error = c_int32(0)
        info = self.getQualifierInfo(name)
        data = numpy.zeros(info.indices[:info.ndim], dtype=numpy.int32,
                           order='F')
        lbuf = c_uint32(info.size)
        status = c_int32(0)
        libddww.ddqget(error, self.diaref, name, status, lbuf, data)
        self._get_error(error, name=name)
        return qualifier(name, status.value, data)

    def getObjectHeader(self, name):
        """ Returns the object header of a given object."""
        if not self.status:
            raise PyddError('Shotfile not open!')
        text = ' ' * 64
        error = c_int32(0)
        buf = numpy.zeros(26, dtype=numpy.int32)
        libddww.ddobjhdr(error, self.diaref, name, buf, text)
        self._get_error(error, name=name)
        relationNames = [self.getObjectName(buf[i]) for i in range(4, 12) if
                         buf[i] != 65535]
        try:
            return __headers__[buf[0]](name, buf,
                                       text.replace('\x00', '').strip(),
                                       relationNames)
        except Exception:
            return objectHeader(name, buf, text.replace('\x00', '').strip(),
                                relationNames)

    def GetObjectHeader(self, name):
        warnings.warn(
            'GetObjectHeader will be removed in the future, please use getObjectHeader.',
            DeprecationWarning)
        return self.getObjectHeader(name)

    def getRelations(self, name):
        """ Returns all relations of a given object."""
        if not self.status:
            raise PyddError('Shotfile not open!')

        rel_out = dd_info()
        head = self.getObjectHeader(name)
        rel_out.error = head.error
        if head.error == 0:
            ids = head.buffer[4:12]
            rel_out.id = []
            rel_out.typ = []
            rel_out.txt = []
            for objid in ids:
                if objid != 65535:
                    rel_out.id.append(objid)
                    tname = self.getObjectName(objid)
                    rel_out.typ.append(self.getObjectValue(tname, 'objtype'))
                    rel_out.txt.append(tname)
        return rel_out

    def GetRelations(self, name):
        warnings.warn(
            'GetRelations will be removed in the future, please use getRelations.',
            DeprecationWarning)
        return self.getRelations(name)

    def getInfo(self, name):
        """ Returns information about the specified signal."""
        if not self.status:
            raise PyddError('Shotfile not open!')

        output = dd_info()
        rel = self.getRelations(name)
        output.rels = rel.txt
        output.error = rel.error
        output.tname = None
        output.aname = None
        output.tlen = None
        output.index = None
        output.units = None
        output.address = None
        output.bytlen = None
        output.level = None
        output.status = None
        output.error = None
        output.ind = None
        if rel.error == 0:
            jtime = None
            jarea = None
            for jid, tid in enumerate(rel.typ):
                if tid == 8:
                    jtime = jid
                    output.tname = rel.txt[jid]
                if tid == 13:
                    jarea = jid
                    output.aname = rel.txt[jid]

            head = self.getObjectHeader(name)
            buf_str = ''
            for hb in head.buffer:
                buf_str += str(hb) + ' '
            output.error = head.error
            if head.error == 0:
                output.buf = head.buffer
                output.objtyp = output.buf[0]
                output.level = output.buf[1]
                output.status = output.buf[2]
                output.error = output.buf[3]
                output.address = output.buf[12]
                output.bytlen = output.buf[13]
                if output.objtyp in (6, 7, 8, 13):
                    output.units = getPhysicalDimension(output.buf[15])
                    output.estatus = output.buf[17]
                    output.fmt = output.buf[14]
                    if output.objtyp in (6, 7):
                        output.index = output.buf[1]
                        dims = numpy.array(output.buf[18:22][::-1],
                                           dtype=numpy.int32)
                        output.ind = numpy.array(dims[dims > 0])

                    if output.objtyp == 8:  # If 'name' is a TB
                        output.tlen = output.buf[21]  # = dims[0]
                        output.tfmt = output.buf[14]
                    else:
                        tlen1 = -1
                        if (output.index == 1) or (output.objtyp == 7):
                            tlen1 = dims[0]
                        elif output.index in (2, 3):
                            tlen1 = dims[1]
                        if jtime is not None:
                            thead = self.getObjectHeader(rel.txt[jtime])
                            tbuf = thead.buffer
                            output.tlen = tbuf[21]
                            output.tfmt = tbuf[14]
                            # Check consistency with TB length
                            if output.tlen != tlen1 and tlen1 != -1:
                                output.tlen = -1
                        else:
                            warnings.warn('No TB found for %s %s'
                                          % (__obj__[output.objtyp], name),
                                          RuntimeWarning)

                    if output.objtyp == 13:  # If 'name' is an AB
                        output.atlen = output.buf[21]
                        output.afmt = output.buf[14]
                        sizes = numpy.array(output.buf[18:21],
                                            dtype=numpy.int32)
                        output.sizes = sizes[sizes > 0]
                    else:
                        # Beware: other than in DDAINFO2, here 'sizes' can have less than
                        # 3 dims, as the 0-sized are removed. Usually (always?) it has 1 dim.
                        if jarea is not None:
                            ahead = self.getObjectHeader(rel.txt[jarea])
                            abuf = ahead.buffer
                            output.atlen = abuf[21]  # #time points of AB
                            output.afmt = abuf[14]
                            sizes = numpy.array(abuf[18:21], dtype=numpy.int32)
                            output.sizes = sizes[sizes > 0]

        return output

    def GetInfo(self, name):
        warnings.warn(
            'GetInfo will be removed in the future, please use getInfo.',
            DeprecationWarning)
        return self.getInfo(name)

    def getObjectData(self, name):
        if not self.status:
            raise PyddError('Shotfile not open.')
        header = self.getObjectHeader(name)
        error = c_int32(0)
        buf = numpy.zeros(header.nBytes, dtype=numpy.int8, order='F')
        buf.dtype = header.dataFormat
        lbuf = c_uint32(buf.size)
        lengb = c_uint32(0)
        libddww.ddobjdata(error, self.diaref, name, lbuf, buf, lengb)
        self._get_error(error, name=name)
        if header.objectType == 'Sig_Group':
            return numpy.reshape(buf, header.indices[::-1], order='C')
        if header.objectType == 'Area_Base':
            order = header.nSteps
            for el in header.sizes:
                order = numpy.append(order, el)
                buf.shape = order
        return buf

    def getRelatingObjects(self, name):
        if not self.status:
            raise PyddError('Shotfile not open')
        names = self.getObjectNames()
        output = []
        for i in names:
            if names[i] != name:
                header = self.getObjectHeader(names[i])
                if name in header.relationNames:
                    output.append(names[i])
        return output
