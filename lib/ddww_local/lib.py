#  -*- coding: utf-8 -*-
# *****************************************************************************
#
# Module authors:
#   Bernhard Sieglin <bernhard.sieglin@ipp.mpg.de>
#   Alexander Lenz <alexander.lenz@ipp.mpg.de>
#
# *****************************************************************************

""" ctypes bindings for libddww """

from ctypes import c_int16, c_uint16, c_int32, c_uint32, c_int64, c_uint64, \
    c_float, c_double, c_char_p, c_void_p, POINTER

import numpy.ctypeslib

__libddww__ = numpy.ctypeslib.load_library('libddww8',
                                           "/afs/ipp/aug/ads/lib64/@sys")
c_int16_p = POINTER(c_int16)
c_uint16_p = POINTER(c_uint16)
c_int32_p = POINTER(c_int32)
c_uint32_p = POINTER(c_uint32)
c_int64_p = POINTER(c_int64)
c_uint64_p = POINTER(c_uint64)
c_float_p = POINTER(c_float)
c_double_p = POINTER(c_double)


def function(argtypes, restype=c_int32):
    return {'argtypes': argtypes, 'restype': restype}


functions = {
    'ddagroup': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(), c_uint32_p]),
    'ddainfo': function([c_int32_p, c_int32, c_char_p,
                         numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint32,
                                                   shape=(3)),
                         numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint32,
                                                   shape=(3)), c_int32_p]),
    'ddainfo2': function(
        [c_int32_p, c_int32, c_char_p, c_uint32_p, c_uint32_p, c_int32_p,
         c_char_p, c_int32_p]),
    'ddccsgnl': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(ndim=1), c_uint32_p, c_int32_p, c_char_p]),
    'ddccsgrp': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(), c_uint32_p, c_int32_p, c_char_p]),
    'ddccxsig': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32_p, c_uint32,
         c_uint32, c_void_p, c_uint32_p, c_int32_p, c_char_p]),
    'ddclose': function([c_int32_p, c_int32]),
    'ddcsgnl': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(ndim=1), c_uint32_p, c_int32_p, c_char_p]),
    'ddcsgrp': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(), c_uint32_p, c_int32_p, c_char_p]),
    'ddcxsig': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32_p, c_uint32,
         c_uint32, numpy.ctypeslib.ndpointer(), c_uint32_p, c_int32_p,
         c_char_p]),
    'ddcshotnr': function([c_char_p, c_char_p, c_uint32, c_uint32_p]),
    'dddelay': function([c_int32_p, c_int32, c_char_p, c_uint32, c_void_p]),
    'dddim': function([c_int32_p, c_int32, c_char_p]),
    'ddevent': function(
        [c_int32_p, c_int32, c_char_p, c_int32, c_void_p, c_uint32, c_void_p,
         c_float_p, c_uint64]),
    'ddflist': function([c_int32_p, c_int32, c_uint32, c_int32_p, c_int32_p]),
    'dddgetsgr': function(
        [c_int32_p, c_char_p, c_char_p, c_uint32, c_int32, c_char_p, c_double,
         c_double, c_char_p, c_double_p, c_double_p, c_uint32_p, c_uint32_p,
         c_uint32, c_char_p]),
    'dddgetsig': function(
        [c_int32_p, c_char_p, c_char_p, c_uint32, c_int32, c_char_p, c_double,
         c_double, c_char_p, c_double_p, c_double_p, c_uint32_p, c_uint32,
         c_char_p]),

    'ddgetsgr': function(
        [c_int32_p, c_char_p, c_char_p, c_uint32, c_int32, c_char_p, c_float,
         c_float, c_char_p, c_float_p, c_float_p, c_uint32_p, c_uint32_p,
         c_uint32, c_char_p]),
    'ddgetsig': function(
        [c_int32_p, c_char_p, c_char_p, c_uint32, c_int32, c_char_p, c_float,
         c_float, c_char_p, c_float_p, c_float_p, c_uint32_p, c_uint32,
         c_char_p]),
    'ddlastshotnr': function([c_int32_p, c_uint32_p]),
    'ddlnames': function([c_int32_p, c_int32, c_char_p, c_int32_p, c_char_p]),
    'ddmapinfo': function([c_int32_p, c_int32, c_char_p,
                           numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.int32),
                           c_char_p, c_int32_p]),
    'ddobjdata': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, numpy.ctypeslib.ndpointer(),
         c_uint32_p]),
    'ddobjhdr': function([c_int32_p, c_int32, c_char_p,
                          numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.int32,
                                                    shape=(26)), c_char_p]),
    'ddobjname': function([c_int32_p, c_int32, c_int32, c_char_p]),
    'ddobjval': function([c_int32_p, c_int32, c_char_p, c_char_p, c_void_p]),
    'ddopen': function(
        [c_int32_p, c_char_p, c_char_p, c_uint32, c_int32, c_int32_p,
         c_char_p]),
    'ddparm': function(
        [c_int32_p, c_int32, c_char_p, c_char_p, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(), c_int32_p]),

    'ddprinfo': function([c_int32_p, c_int32, c_char_p, c_int32_p, c_char_p,
                          numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint32),
                          numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint16),
                          numpy.ctypeslib.ndpointer(ndim=1,
                                                    dtype=numpy.int32)]),
    'dd_prinfo': function(
        [c_int32_p, c_int32, c_char_p, c_char_p, c_uint32_p, c_uint16_p]),
    'ddqget': function([c_int32_p, c_int32, c_char_p, c_int32_p, c_uint32_p,
                        numpy.ctypeslib.ndpointer(dtype=numpy.int32)]),
    'ddqinfo': function([c_int32_p, c_int32, c_char_p, c_int32_p,
                         numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint32,
                                                   shape=(3)), c_uint32_p]),
    'ddsgroup': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(), c_uint32_p]),
    'ddshot_prefetch': function(
        [c_int32_p, c_char_p, c_char_p, c_uint32_p, c_int32, c_uint32,
         c_int32]),
    'ddsignal': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(ndim=1), c_uint32_p]),
    'ddsinfo': function([c_int32_p, c_int32, c_char_p, c_uint32_p, c_char_p,
                         numpy.ctypeslib.ndpointer(ndim=1, dtype=numpy.uint32,
                                                   shape=(4))]),
    'ddtbase': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(ndim=1), c_uint32_p]),
    'ddtindex': function(
        [c_int32_p, c_int32, c_char_p, c_float, c_float, c_uint32_p,
         c_uint32_p]),
    'dddtindex': function(
        [c_int32_p, c_int32, c_char_p, c_double, c_double, c_uint32_p,
         c_uint32_p]),
    'ddtindexneu': function(
        [c_int32_p, c_int32, c_char_p, c_float, c_float, c_uint32_p,
         c_uint32_p]),
    'dddtindexneu': function(
        [c_int32_p, c_int32, c_char_p, c_double, c_double, c_uint32_p,
         c_uint32_p]),

    'ddtmout': function([c_int32_p, c_int32]),
    'ddtrange': function(
        [c_int32_p, c_int32, c_char_p, c_float_p, c_float_p, c_uint32_p,
         c_uint32_p]),
    'dddtrange': function(
        [c_int32_p, c_int32, c_char_p, c_double_p, c_double_p, c_uint32_p,
         c_uint32_p]),
    'dduprmd': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_float_p, c_int32_p,
         c_int32_p, c_int32_p, c_uint32_p]),
    'dduprot': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_float_p, c_int32_p,
         c_uint32_p, c_uint32_p]),
    'ddwait': function([c_int32_p, c_uint32_p]),
    'ddwaitt': function([c_int32_p, c_uint32_p, c_int32]),
    'ddxtrsignal': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32_p, c_uint32,
         c_uint32, numpy.ctypeslib.ndpointer(), c_uint32_p]),
    'wwainsert': function(
        [c_int32_p, c_int32, c_char_p, c_uint32, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(),
         numpy.ctypeslib.ndpointer(dtype=numpy.uint32, ndim=1, shape=(3))]),
    'wwclear': function([c_int32_p, c_int32, c_char_p]),
    'wwclose': function([c_int32_p, c_int32, c_char_p, c_char_p]),
    'wwinsert': function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32,
                          numpy.ctypeslib.ndpointer(), c_uint32,
                          numpy.ctypeslib.ndpointer(dtype=numpy.uint32, ndim=1,
                                                    shape=(3))]),
    'wwoinfo': function([c_int32_p, c_int32, c_char_p, c_uint32_p,
                         numpy.ctypeslib.ndpointer(dtype=numpy.uint16),
                         c_uint32_p,
                         numpy.ctypeslib.ndpointer(dtype=numpy.int32),
                         numpy.ctypeslib.ndpointer(dtype=numpy.uint32)]),
    'wwonline': function([c_int32_p, c_char_p, c_char_p, c_int32]),
    'wwopen': function(
        [c_int32_p, c_char_p, c_char_p, c_uint32, c_char_p, c_int32_p,
         c_int32_p, c_char_p]),
    'wwparm': function(
        [c_int32_p, c_int32, c_char_p, c_char_p, c_uint32, c_uint32,
         numpy.ctypeslib.ndpointer(), c_uint32]),
    'wwpred': function(
        [c_int32_p, c_int32, c_char_p, c_char_p, c_uint32, c_int32]),
    'wwqset': function(
        [c_int32_p, c_int32, c_char_p, c_int16, c_uint32, c_int32]),
    'wwsfile': function([c_int32_p, c_int32, c_int32]),
    'wwsignal': function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32,
                          numpy.ctypeslib.ndpointer(), c_uint32]),
    'wwtbase': function([c_int32_p, c_int32, c_char_p, c_uint32, c_uint32,
                         numpy.ctypeslib.ndpointer(), c_uint32]),
    'wwtext': function([c_int32_p, c_int32, c_char_p, c_char_p]),

    'xxcom': function([c_int32]),
    'xxerrprt': function([c_int32, c_char_p, c_int32, c_uint32, c_char_p]),
    'xxerror': function([c_int32, c_uint32, c_char_p]),
    'xxnote': function([c_int32]),
    'xxsev': function([c_int32]),
    'xxwarn': function([c_int32])
}

class Library(object):
    """Wrapping class for ctype magic and py2/py3 compatibility."""
    def __init__(self, raw_lib, func_defs):
        self._raw = {}

        for name, definition in func_defs.items():
            self._raw[name] = raw_lib[name]
            self._raw[name].argtypes = definition['argtypes']
            self._raw[name].restype = definition['restype']

            self.wrap(name)


    def wrap(self, func_name):
        def wrapper(*args):
            conv_args = []
            for entry in args:
                # check for strings and encode them (py3 compatiblity)
                if isinstance(entry, str):
                    try:
                        # Check for byte strings.
                        # If it's already a byte string (so it has a decode)
                        # don't do anything as it's probably an buffer which
                        # will be written to.
                        entry.decode()
                    except Exception:
                        entry = entry.encode()
                conv_args.append(entry)
            args = tuple(conv_args)

            return self._raw[func_name](*args)
        wrapper.__name__ = func_name

        setattr(self, func_name, wrapper)

lib = Library(__libddww__, functions)
