#  -*- coding: utf-8 -*-
# *****************************************************************************
#
# Module authors:
#   Bernhard Sieglin <bernhard.sieglin@ipp.mpg.de>
#   Alexander Lenz <alexander.lenz@ipp.mpg.de>
#
# *****************************************************************************

from ctypes import c_int32, c_uint32

import numpy
from numpy import dtype
import warnings

from ddww.lib import lib as libddww

__type__ = {numpy.int32: 1, numpy.float32: 2, numpy.float64: 3,
            numpy.complex: 4, numpy.bool: 5,
            numpy.byte: 6, numpy.int64: 10, numpy.int16: 11, numpy.uint16: 12,
            numpy.uint32: 13,
            numpy.uint64: 14, numpy.character: 6, numpy.dtype('S8'): 6,
            numpy.dtype('S16'): 6,
            numpy.dtype('S32'): 6, numpy.dtype('S64'): 6}

__type__ = {
    dtype(key): value
    for key, value in __type__.items()
}


def get_error(error):
    """ Check if an error/warning occured. """
    isError = libddww.xxsev(error) == 1
    isWarning = libddww.xxwarn(error) == 1
    if isError or isWarning:
        text = ' '*256
        libddww.xxerrprt(-1, text, error, 3, '')
        if isError:
            raise Exception(text.replace('\x00','').strip())
        else:
            warnings.warn(text.replace('\x00','').strip(), RuntimeWarning)



class shotfile(object):
    """ Class to write ASDEX Upgrade shotfiles using libddww8. """
    def __init__(self, diagnostic=None, pulseNumber=None, experiment=None, mode='new', edition=-1):
        """ Initialize shotfile.

        Arguments:
        experiment -- Name of the experiment / user for the shotfile to be created under.
        diagnostic -- Name of the shotfile
        pulseNumber -- Corresponding ASDEX Upgrade shot number.

        Keyword Arguments
        mode -- Open / create mode of the shotfile.
            'new': Create an new shotfile
            'work': Open and existing shotfile
        edition -- Specify which edition to open.
            -1 : Create new edition (default)
            0 : Open last edition
            > 0: Open specified edition
        """
        object.__init__(self)
        error = c_int32(0)
        self.diaref = c_int32(0)
        if diagnostic is not None and pulseNumber is not None and experiment is not None:
            self.open(experiment, diagnostic, pulseNumber, mode=mode, edition=edition)

    def open(self, experiment, diagnostic, pulseNumber, mode='new', edition=-1):
        self.close()
        error = c_int32(0)
        edition = c_int32(edition)
        time = ' '*18
        libddww.wwopen(error, experiment, diagnostic, pulseNumber, mode, edition, self.diaref, time)
        get_error(error.value)
        self.experiment = experiment
        self.diagnostic = diagnostic
        self.pulseNumber = pulseNumber
        self.edition = edition
        self.time = time
        return error.value

    def close(self, disp='work', space='maxspace'):
        if self.status:
            error = c_int32(0)
            libddww.wwclose(error, self.diaref, disp, space)
            self.diaref.value = 0
            del self.experiment
            del self.diagnostic
            del self.pulseNumber
            del self.edition
            del self.time

    def set_timebase(self, name, time):
        error = c_int32(0)
        typ = c_uint32(__type__[time.dtype])
        libddww.wwtbase(error, self.diaref, name, __type__[time.dtype], time.size, time, 1)
        return error.value

    def set_signal(self, name, data):
        error = c_int32(0)
        typ = c_uint32(__type__[data.dtype])
        libddww.wwsignal(error, self.diaref, name, __type__[data.dtype], data.size, data, 1)
        return error.value

    def get_object_info(self, name):
        error = c_int32(0)
        typ = c_uint32(0)
        form = numpy.zeros(256, dtype=numpy.uint16)
        ntval = c_uint32(0)
        items = numpy.zeros(256, dtype=numpy.int32)
        indices = numpy.zeros(3, dtype=numpy.uint32)
        libddww.wwoinfo(error, self.diaref, name, typ, form, ntval, items, indices)
        return error.value, typ.value, form[form != 0], ntval.value, items[form != 0], indices

    def clear(self, name):
        error = c_int32(0)
        libddww.wwclear(error, self.diaref, name)
        return error.value

    def set_text(self, name, text):
        error = c_int32(0)
        libddww.wwtext(error, self.diaref, name, text)
        return error.value

    def set_signal_group(self, name, data):
        error = c_int32(0)
        oldShape = data.shape
        if len(oldShape) != 4:
            shape = numpy.append(numpy.uint32(oldShape), numpy.ones(4 - len(oldShape), dtype=numpy.uint32))
            data.shape = shape
        length, N1, N2, N3 = data.shape
        for x in xrange(N1):
            for y in xrange(N2):
                for z in xrange(N3):
                    libddww.wwinsert(error, self.diaref, name, __type__[data.dtype], length, data[:,x,y,z].copy(), 1, numpy.uint32([x+1,y+1,z+1]))
        data.shape = oldShape
        return error.value

    def set_area_base(self, name, x, y = None ,z = None):
        error = c_int32(0)
        data = x
        if y is not None:
            data = numpy.append(data.flatten(), y.flatten())
        if z is not None:
            data = numpy.append(data.flatten(), z.flatten())
        libddww.wwainsert(error, self.diaref, name, 1, x.shape[0], __type__[data.dtype], data, numpy.uint32([x.shape[1], 0 if y is None else y.shape[1], 0 if z is None else z.shape[1]]))
        return error.value

    def set_parameter(self, set_name, parameter_name, data):
        error = c_int32(0)
        if not isinstance(data, numpy.ndarray):
            if isinstance(data, float):
                data = numpy.float32([data])
            elif isinstance(data, int):
                data = numpy.int32([data])
            elif isinstance(data, list):
                if isinstance(data[0], float):
                    data = numpy.float32(data)
                elif isinstance(data[0], int):
                    data = numpy.int32(data)
                else:
                    raise Exception('Unsupported data type in list %s' % type(data[0]))
            else:
                raise Exception('Unsupported data type %s' % type(data))
        if data.dtype == numpy.dtype('S8'):
            libddww.wwparm(error, self.diaref, set_name, parameter_name, __type__[data.dtype], data.size*8, data, 1)
            return error.value
        libddww.wwparm(error, self.diaref, set_name, parameter_name, __type__[data.dtype], data.size, data, 1)
        return error.value

    @property
    def status(self):
        return self.diaref.value != 0
