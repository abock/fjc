#!/usr/bin/env python

#try:
#from PyQt4 import QtGui, QtCore # Import the PyQt4 module we'll need
#except:
from PyQt5 import QtGui, QtCore, QtWidgets # Import the PyQt5 module we'll need
import sys # We need sys so that we can pass argv to QApplication

import lib.main_window as main_window # This file holds our MainWindow and all main_window related things
              # it also keeps events etc that we defined in Qt Designer
from functools import partial
import lib.pyqtgraph as pg
import numpy as np

from aug_sfutils import SFREAD, EQU, getlastshot
from lib.local_ww import write_sf
from lib.pyqtgraph.parametertree import Parameter, ParameterTree
from IPython import embed
from scipy.interpolate import interp1d
#from scipy.signal import argrelextrema
#from lib.map_equ_local import equ_map as em
#import threading
import time
from lib.vta2dcn import vta2dcn
#import getpass
from lib.settings import Settings
import traceback
from scipy import optimize, signal
import copy
from scipy.signal import argrelextrema, medfilt

'''
from tensorflow import keras
import tensorflow as tf

#keras.models.load_model('200k-200-50-15-5.h5')
model = keras.models.load_model('lib/200k-200-50-15-5.h5')

for offset in offsets[:-2]:
    probe = Hx[:,offset+1:offset+198]
    if probe.shape == (5, 197):
        results.append( model.predict(np.array([probe]))[0] )
        rt.append(t[offset+98])

'''



fringe_ne = 0.572e19

class Bunch(object):
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

class RunDetectorNNThread(QtCore.QThread):
    prediction_finished = QtCore.pyqtSignal(object)
    model = None

    def __init__(self, parent):
        QtCore.QThread.__init__(self)
        self.parent = parent
        #from tensorflow import keras
        #import tensorflow as tf
        #self.model = keras.models.load_model('lib/200k-200-50-15-5.h5')

    def run(self):
        return
        print('predicting...')
        self.prediction_finished.emit('RunDetectorNN completed')
        pass


class VTA2DCNThread(QtCore.QThread):
    modelling_finished = QtCore.pyqtSignal(object)

    def __init__(self, parent):
        QtCore.QThread.__init__(self)
        self.parent = parent

    def run(self):
        time.sleep(1)
        shot = self.parent.loaded_shot
        equil_candidates = [('AUGD','IDE'), ('AUGI','IDE'), ('AUGD','EQH'), 
                            ('AUGD','EQR'), ('AUGD','EQI')]
        for exp, diag in equil_candidates:
            print('trying...', exp, diag)
            eq = EQU(shot, exp=exp, diag=diag)
            if eq.sf.status:
                break
        vta = SFREAD('VTA', shot)
        dcn = SFREAD('DCN', shot)
        vta_modelled = vta2dcn(vta, dcn, eq, method='nearest', rescale=True)
        self.parent.vta_modelled = {}
        for key in vta_modelled:
            new_key = key-1 if key != 0 else 2
            self.parent.vta_modelled[new_key] = vta_modelled[key]
        self.modelling_finished.emit('VTA2DCN completed')


class LoadBRT1Thread(QtCore.QThread):
    loading_finished = QtCore.pyqtSignal(object)

    def __init__(self, parent):
        QtCore.QThread.__init__(self)
        self.parent = parent

    def run(self):
        time.sleep(1)
        shot = self.parent.loaded_shot
        brd = SFREAD('BRD', shot)  
        self.parent.brt1_trace = Bunch(data=np.abs(brd.getobject("BRT1",tend=13))**0.5, time=brd.gettimebase("BRT1",tend=13))
        self.parent.brt1_trace.data -= np.mean(self.parent.brt1_trace.data[self.parent.brt1_trace.time<0.2])
        self.parent.brt1_trace.data /= self.parent.brt1_trace.data.mean()

        self.loading_finished.emit('BRT1 loaded')
    

class LoadPelletsThread(QtCore.QThread):
    loading_finished = QtCore.pyqtSignal(object)

    def __init__(self, parent):
        QtCore.QThread.__init__(self)
        self.parent = parent

    def run(self):
        time.sleep(1)
        shot = self.parent.loaded_shot
        pid = SFREAD('PAC', shot) if shot > 41678 else SFREAD('PID', shot) if shot > 34995 else SFREAD('PEL', shot)
        self.parent.pellet_trace = Bunch(data=pid.getobject("5Co",tend=13), time=pid.gettimebase("5Co",tend=13))
        self.parent.pellet_trace.data -= np.mean(self.parent.pellet_trace.data)
        self.parent.pellet_trace.data /= self.parent.pellet_trace.data.max()

        stride = 100
        tmp = self.parent.pellet_trace.data[::stride]
        tmp2 = (tmp > 0.15).astype(float)
        tmp3 = np.append(np.diff(tmp2), 0)
        pellet_indices = np.arange(len(tmp3))[tmp3 == 1]
        dead_mask = np.ones_like(tmp3)
        for pi in pellet_indices:
            dead_mask[pi+1:pi+200] = 0
        self.parent.pellet_times = self.parent.pellet_trace.time[
            np.arange(len(tmp3))[
                tmp3*dead_mask == 1
                ]
             * stride]
        self.parent.pellet_times = self.parent.pellet_times[self.parent.pellet_times < 12]

        #trigger = pid('RTSP')
        #mask = np.diff(trigger.data) > 0.1
        self.loading_finished.emit('Pellet times loaded')
        #self.parent.pellet_times = trigger.time[mask]

class App(QtWidgets.QMainWindow, main_window.Ui_MainWindow):
    loaded_shot = None
    vta_modelled = None
    pellet_times = None
    data = None
    exclude_mode = False
    show_comparison_chords = False
    show_NN_detection = False
    NN_estimate = None

    channels = [1, 2, 0, 4, 5]

    settings = Settings()

    def embed(self):
        embed()

    def __init__(self):
        QtCore.pyqtRemoveInputHook() # to enable ipython embed

        super(self.__class__, self).__init__()
        self.setupUi(self)  # This is defined in main_window.py file automatically
                            # It sets up layout and widgets that are defined
        self.actionQuit.triggered.connect(self.close_application)
        self.actionNew.triggered.connect(self.new)
        self.actionNew_from_CON.triggered.connect(self.new_from_CON)
        self.actionNew_from_DCK.triggered.connect(self.new_from_DCK)
        #self.actionNew_from_DCS.triggered.connect(self.new_from_DCS)
        self.actionSave.triggered.connect(self.save)
        self.actionEmbed.triggered.connect(self.embed)
        self.actionShow_Thomson_Estimate.triggered.connect(self.show_thomson)
        self.actionShow_Pellet_Times.triggered.connect(self.show_pellet)
        self.actionShow_BRT1.triggered.connect(self.show_brt1)
        self.actionExclude_Interval.triggered.connect(self.exclude_interval)
        self.actionExclude_Interval_all_chords.triggered.connect(self.exclude_interval_all)
        self.actionLoad_Correction.triggered.connect(self.load_correction_prompt)
        self.actionShow_Comparison_Chords.triggered.connect(self.toggle_comparison_chords)
        self.actionAdd_corrections_for_pellets.triggered.connect(self.fix_pellets_base)
        self.actionAdd_Obvious_Corrections.triggered.connect(self.fix_obvious_base)
        self.actionShow_Neural_Network_Detection.triggered.connect(self.toggle_NN_detection)
        self.actionClear_All_Corrections.triggered.connect(self.clear_all_corrections)

        self.setWindowTitle('Fringe Jump Corrector')

        self.tab_names = ['tabH%i'%i for i in [1,2,3,4,5]] + ['tabSummary']
        self.tabHs = [self.__dict__[key] for key in self.tab_names]

        self.plots1 = {}
        self.plots2 = {}
        self.fjpt = {}
        self.fjpt_parameters = {}

        self.NN_modeller = RunDetectorNNThread(self)
        self.NN_modeller.prediction_finished.connect(self.on_prediction_ready)

        for i, tab in enumerate(self.tabHs):
            hbox = QtWidgets.QHBoxLayout(tab)

            pt = ParameterTree()
            self.fjpt[i] = pt
            self.fjpt_parameters[i] = Parameter.create(name='Corrections', type='group')
            self.fjpt[i].setParameters(self.fjpt_parameters[i], showTop=False)

            pw1 = pg.PlotWidget(name='Plot_%s_1'%self.tab_names[i])
            pw1.getPlotItem().showGrid(x=True, y=True)#, alpha=None)
            self.plots1[i] = pw1

            pw2 = pg.PlotWidget(name='Plot_%s_2'%self.tab_names[i])
            pw2.getPlotItem().showGrid(x=True, y=True)#, alpha=None)
            pw2.setXLink('Plot_%s_1'%self.tab_names[i])
            self.plots2[i] = pw2

            vsplitter = QtWidgets.QSplitter(QtCore.Qt.Vertical)
            vsplitter.addWidget(pw1)
            vsplitter.addWidget(pw2)

            if i < 5:
                hsplitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
                hsplitter.addWidget(pt)
                hsplitter.addWidget(vsplitter)
                hbox.addWidget(hsplitter)
            else:
                hbox.addWidget(vsplitter)

            vsplitter.setSizes([10000,4000])
            hsplitter.setSizes([3500,10000])

            def mouseClicked(evt, clicked_channel=None, plot=None):
                vb = plot.getPlotItem().vb # if plot is not None else vb
                t = vb.mapSceneToView(evt.scenePos()).x()
                modifiers = QtWidgets.QApplication.keyboardModifiers()
                if self.exclude_mode:
                    self.add_correction(channel=clicked_channel, t=t, fringes=None, dt=15.0)
                    self.exclude_mode = False
                elif modifiers == QtCore.Qt.ShiftModifier:
                    #print('Shift+Click')
                    self.add_correction(channel=clicked_channel, t=t, fringes=1, dt=1e-3)
                elif modifiers == QtCore.Qt.ControlModifier:
                    #print('Control+Click')
                    self.add_correction(channel=clicked_channel, t=t, fringes=-1, dt=1e-3)
                elif modifiers == (QtCore.Qt.ControlModifier |
                                   QtCore.Qt.ShiftModifier):
                    #print('Control+Shift+Click')
                    for i in range(5):
                        self.add_correction(channel=i, t=t, fringes=0, dt=1e-3)
                    pass
                else:
                    pass
            if i < 5:
                local_mouseClicked = partial(mouseClicked, clicked_channel=i, plot=pw1)
                pw1.getPlotItem().scene().sigMouseClicked.connect(local_mouseClicked)

                local_mouseClicked2 = partial(mouseClicked, clicked_channel=i, plot=pw2)
                pw2.getPlotItem().scene().sigMouseClicked.connect(local_mouseClicked2)

        #self.new(shot=33255)#, source='CON')
        #self.load_correction('AUGD', 33255, 0)
        #self.clear_all_corrections()
        #self.show_pellet()
        #while self.pellet_times is None:
        #    time.sleep(1)
        #self.fix_pellets_base()
        pass

    def toggle_comparison_chords(self):
        self.show_comparison_chords = self.actionShow_Comparison_Chords.isChecked()
        self.draw()

    def toggle_NN_detection(self):
        self.show_NN_detection = self.actionShow_Neural_Network_Detection.isChecked()
        self.draw()

    def suggest_corrections(self):
        def butter_lowpass(highcut, Fs, order=2):
            nyq = 0.5 * Fs
            #low = lowcut / nyq
            high = highcut / nyq
            b, a = signal.butter(order, high, btype='low')
            return b, a

        def butter_lowpass_filter(data, highcut, Fs, order=2):
            b, a = butter_lowpass(highcut, Fs, order=order)
            y = signal.filtfilt(b, a, data)
            return y

        import matplotlib.pyplot as plt
        #plt.ion()
        plt.figure(1)

        def function(x, m, t):
            return m*x+t

        def leastsq_function(params, *args):
            m = params[0]
            t1 = params[1]
            t2 = params[2]
            x = args[0]
            y = args[1]
            n = len(x)/2

            yfit = np.empty(x.shape)
            yfit[:n] = function(x[:n], m, t1)
            yfit[n:] = function(x[n:], m, t2)

            return y - yfit


        n_firstlastsamples = 20 # 50
        n_skipsamples = 20

        ress = []

        for key in self.data:
            tvec, dvec = self.data[key].time, self.data[key].data/fringe_ne
            color = plt.plot(tvec, dvec)[0].get_color()

            #Fs = 10e3
            #dvec = butter_lowpass_filter(dvec, 30, Fs, order=2)
            #plt.plot(tvec, dvec, color=color, lw=2)

            i = 0
            while i < len(tvec) - 2*n_firstlastsamples - n_skipsamples:
                slice1 = dvec[i:i+n_firstlastsamples]
                slice2 = dvec[i+n_firstlastsamples+n_skipsamples:i+2*n_firstlastsamples+n_skipsamples]
                slicebetween = dvec[i+n_firstlastsamples:i+n_firstlastsamples+n_skipsamples]

                db = slicebetween.max() - slicebetween.min()

                if db > 0.7:

                    tslice1 = tvec[i:i+n_firstlastsamples]
                    tslice2 = tvec[i+n_firstlastsamples+n_skipsamples:i+2*n_firstlastsamples+n_skipsamples]
                    #avg1 = np.average(slice1)
                    #avg2 = np.average(slice2)

                    m = t1 = t2 = 0
                    params0 = [m, t1, t2]
                    args = (np.append(tslice1, tslice2), np.append(slice1, slice2))
                    result = optimize.leastsq(leastsq_function, params0, args=args, full_output=True)
                    m, t1, t2 = result[0]
                    #ierr = result[-1]
                    infodict = result[2]

                    x = np.append(tslice1, tslice2)
                    y = np.append(slice1, slice2)
                    args = (x, y)
                    res = leastsq_function(result[0], *args)
                    res = np.dot(res, res) #/(2*n_firstlastsamples)

                    ress.append(res)


                    ss_err=(infodict['fvec']**2).sum()
                    ss_tot=((y-y.mean())**2).sum()
                    rsquared=1-(ss_err/ss_tot)


                    delta = (t2-t1)
                    if np.abs(delta) > 0.8 and rsquared > 0.6:
                    #if np.abs(delta) > 0.7 and rsquared > 0.:
                        print(key, tvec[i], delta, res)


                        #embed()

                        plt.axvspan(tvec[i+n_firstlastsamples], tvec[i+n_firstlastsamples+n_skipsamples],
                            color=color, alpha=0.2)

                        plt.plot(tslice1, m*tslice1+t1, color=color)
                        plt.plot(tslice2, m*tslice2+t2, color=color)

                        plt.text(tvec[i+n_firstlastsamples], m*tvec[i+n_firstlastsamples]+t1,
                            "delta: %3.2f r2=%3.2f db=%3.2f"%(delta,rsquared, db))

                        #fitfunc1 = np.poly1d(np.polyfit(tslice1, slice1, 1))
                        #plt.plot(tslice1, fitfunc1(tslice1), color=color)
                        #fitfunc2 = np.poly1d(np.polyfit(tslice2, slice2, 1))
                        #plt.plot(tslice2, fitfunc2(tslice2), color=color)

                        #embed()

                        if True: i += n_firstlastsamples + n_skipsamples

                i += 1 # n_skipsamples/2

        plt.grid(True)
        plt.tight_layout()
        #plt.xlim(4,10)
        plt.show()

        sys.exit()
        pass

    def load_correction_prompt(self):
        if self.loaded_shot is None:
            return

        d = QtWidgets.QDialog()
        ok = QtWidgets.QPushButton("OK",d)
        x0, y0 = 10, 40

        exb = QtWidgets.QLineEdit(d)
        exb.setFixedWidth(80)
        exb.move(x0, y0-30)
        exb.setText(self.settings.corrections_load_exp)

        shb = QtWidgets.QSpinBox(d)
        shb.setFixedWidth(70)
        shb.move(x0+90, y0-30)
        shb.setMaximum(999999)
        shb.setValue(self.loaded_shot)

        edb = QtWidgets.QSpinBox(d)
        #edb.setFixedWidth(20)
        edb.move(x0+170, y0-30)
        edb.setMaximum(999)

        def load_correction():
            exp = str(exb.text())
            shot = shb.value()
            ed = edb.value()
            self.load_correction(exp, shot, ed)
            d.close()
            self.settings.corrections_load_exp = exp

        ok.move(x0+55,y0)
        ok.clicked.connect(load_correction)

        ca = QtWidgets.QPushButton("Cancel",d)
        ca.move(x0+82+55,y0)
        ca.clicked.connect(d.close)

        d.setWindowTitle("Load DCK correction")
        d.setWindowModality(QtCore.Qt.ApplicationModal)
        d.exec_()

    def load_correction(self, exp, shot, ed):
        #embed()
        dck = SFREAD('DCK', shot, experiment=exp, edition=ed)
        channels = self.channels
        for i, ch in enumerate(channels):
            psname = 'ParH-%i'%ch
            #embed()
            #print(psname)
            parset = dck.getparset(psname)
            dte = parset['DT_Exclu'] if 'DT_Exclu' in parset else []
            tes = parset['T_Exclud'] if 'T_Exclud' in parset else []
            nf = parset['N_Fehler'] if 'N_Fehler' in parset else 0
            ts = parset['T_Fehler'] if 'T_Fehler' in parset else []
            dts = parset['DT_Fehle'] if 'DT_Fehle' in parset else []
            nfringes = parset['Fehler'] if 'Fehler' in parset else []
            #print pset.keys()
            if sum(dte) > 0:
                for j in range(len(dte)):
                    dt = dte[j]
                    t = tes[j]
                    #print(j, t, dt)
                    if dt == 0: continue
                    self.add_correction(i, t, None, dt, UI_update=False)
                pass
            for j in range(nf):
                t = ts[j]
                dt = dts[j]
                fringes = nfringes[j]
                #print t, dt, fringes
                self.add_correction(i, t, fringes, dt, UI_update=False)

        self.update_corrections()
        self.draw()
        #self.update_corrections_UI()

    def exclude_interval_all(self):
        if self.loaded_shot is None:
            return

        d = QtWidgets.QDialog()
        ok = QtWidgets.QPushButton("OK",d)
        x0, y0 = 10, 40

        tb1 = QtWidgets.QLabel(d)
        tb1.move(x0+25, y0-25)
        tb1.setText("t0:")


        t0b = QtWidgets.QDoubleSpinBox(d)
        t0b.move(x0+50, y0-30)
        t0b.setDecimals(3)
        t0b.setMaximum(20)
        t0b.setSingleStep(0.001)


        tb2 = QtWidgets.QLabel(d)
        tb2.move(x0+150, y0-25)
        tb2.setText("dt:")

        dtb = QtWidgets.QDoubleSpinBox(d)
        dtb.move(x0+175, y0-30)
        dtb.setMaximum(20)
        dtb.setSingleStep(0.001)
        dtb.setValue(13)

        def add_exclusion():
            t0 = t0b.value()
            dt = dtb.value()
            for i, ch in enumerate(self.channels):
                self.add_correction(i, t0, None, dt, UI_update=False)
            self.update_corrections()
            self.draw()
            d.close()

        ok.move(x0+55,y0)
        ok.clicked.connect(add_exclusion)

        ca = QtWidgets.QPushButton("Cancel",d)
        ca.move(x0+82+55,y0)
        ca.clicked.connect(d.close)

        d.setTabOrder(t0b, dtb)
        d.setTabOrder(dtb, ok)
        d.setTabOrder(ok, ca)

        d.setWindowTitle("Exclude interval on all chords")
        d.setWindowModality(QtCore.Qt.ApplicationModal)
        t0b.selectAll()
        d.exec_()
        pass

    def exclude_interval(self):
        self.statusbar.showMessage("Click to exclude interval.")
        self.exclude_mode = True
        pass

    def show_pellet(self):
        if self.loaded_shot is None:
            return
        self.PID_loader = LoadPelletsThread(self)
        self.PID_loader.loading_finished.connect(self.on_pellet_ready)
        self.PID_loader.start()
        pass

    def show_brt1(self):
        if self.loaded_shot is None:
            return
        self.BRT1_loader = LoadBRT1Thread(self)
        self.BRT1_loader.loading_finished.connect(self.on_pellet_ready)
        self.BRT1_loader.start()


    def on_pellet_ready(self):
        self.draw()
        pass

    def on_prediction_ready(self):
        pass

    def fix_obvious_base(self):
        d = QtWidgets.QDialog()
        d.resize(300,220)

        ok = QtWidgets.QPushButton("OK",d)
        x0, y0 = 50, 10

        tb0 = QtWidgets.QLabel(d)
        tb0.move(x0+35, y0)
        tb0.setText("From [s]")

        tb0 = QtWidgets.QLabel(d)
        tb0.move(x0+130, y0)
        tb0.setText("To [s]")

        channels = [1,2,0,4,5]
        cbs, t0s, t1s = [], [], []
        for i in range(5):
            cbn = QtWidgets.QCheckBox(d)
            cbn.move(x0-20, y0+25+30*i)
            cbs.append(cbn)

            tbn = QtWidgets.QLabel(d)
            tbn.move(x0, y0+25+30*i)
            tbn.setText("H-%i"%channels[i])

            t0b = QtWidgets.QDoubleSpinBox(d)
            t0b.move(x0+35,  y0+20+30*i)
            t0b.setDecimals(3)
            t0b.setMinimum(-0.5)
            t0b.setMaximum(13)
            t0b.setValue(-0.5)
            t0b.setSingleStep(0.001)
            t0s.append(t0b)

            t1b = QtWidgets.QDoubleSpinBox(d)
            t1b.move(x0+130,  y0+20+30*i)
            t1b.setDecimals(3)
            t1b.setMinimum(-0.5)
            t1b.setMaximum(13)
            t1b.setValue(13)
            t1b.setSingleStep(0.001)
            t1s.append(t1b)

        def fix_obvious():
            # this fixes FJs that are clearly visible when moving a median filter over the data
            # and observing the gradient ... jumps over 1/10 of a fringe are flagged for correction
            for channel, cb, t0, t1 in zip(self.corrections, cbs, t0s, t1s):
                if cb.checkState() == 0:
                    continue
                print("Fixing H-%i..."%self.channels[channel])
                n = 0
                grd = np.gradient(medfilt(self.data[channel].data+self.FJcorrection[i]*0.572e19, 101))
                grd += np.random.rand(len(grd))*1e16 # this prevents losing extrema because two points happen to be equal
                for x in argrelextrema(grd, np.less)[0]:
                    if np.abs(grd[x]) > 0.5e18 and t0.value() < self.data[channel].time[x] and self.data[channel].time[x] < t1.value():
                        self.add_correction(channel, self.data[channel].time[x], fringes=1, UI_update=False)
                        n += 1
                for x in argrelextrema(grd, np.greater)[0]:
                    if np.abs(grd[x]) > 0.5e18 and t0.value() < self.data[channel].time[x] and self.data[channel].time[x] < t1.value():
                        self.add_correction(channel, self.data[channel].time[x], fringes=-1, UI_update=False)
                        n += 1
                print("Fixed %i FJs"%n)
                pass
            self.update_corrections()
            self.draw()
            d.close()
            pass

        x0 = 30

        ok.move(x0+55,y0+180)
        ok.clicked.connect(fix_obvious)

        ca = QtWidgets.QPushButton("Cancel",d)
        ca.move(x0+82+55,y0+180)
        ca.clicked.connect(d.close)


        d.setWindowTitle("Add obvious corrections")
        d.setWindowModality(QtCore.Qt.ApplicationModal)
        d.exec_()

        pass

    def fix_pellets_base(self):
        if self.loaded_shot is None or self.pellet_times is None:
            print('are pellet times loaded yet?')
            return
        d = QtWidgets.QDialog()
        d.resize(350,220)

        ok = QtWidgets.QPushButton("OK",d)
        x0, y0 = 10, 10

        tb0 = QtWidgets.QLabel(d)
        tb0.move(x0, y0)
        tb0.setText("Detected %i pellets between %3.2fs and %3.2fs."%(
            len(self.pellet_times), self.pellet_times.min(), self.pellet_times.max()
        ))

        x0 = 50
        channels = [1,2,0,4,5]
        inbs = []
        for i in range(5):
            tbn = QtWidgets.QLabel(d)
            tbn.move(x0, y0+25+30*i)
            tbn.setText("H-%i"%channels[i])

            inb = QtWidgets.QSpinBox(d)
            inb.move(x0+35, y0+20+30*i)
            inb.setValue(0)
            inb.setMaximum(9)
            inb.setMinimum(-9)
            inbs.append(inb)

        def fix_pellets():
            print("Applying corrections...")
            for i, inb in enumerate(inbs):
                n = int(inb.value())
                print("%i: %i"%(channels[i], n))
                if n == 0: continue
                for pt in self.pellet_times:
                    self.add_correction(i, pt, fringes=n, UI_update=False)
            self.update_corrections()
            self.draw()
            d.close()
            pass

        x0 = 30

        ok.move(x0+55,y0+180)
        ok.clicked.connect(fix_pellets)

        ca = QtWidgets.QPushButton("Cancel",d)
        ca.move(x0+82+55,y0+180)
        ca.clicked.connect(d.close)


        d.setWindowTitle("Apply batch pellet correction")
        d.setWindowModality(QtCore.Qt.ApplicationModal)
        d.exec_()

        pass

    def clear_all_corrections(self):
        d = QtWidgets.QDialog()
        d.resize(300,220)

        ok = QtWidgets.QPushButton("OK",d)
        x0, y0 = 50, 10

        tb0 = QtWidgets.QLabel(d)
        tb0.move(x0+35, y0)
        tb0.setText("From [s]")

        tb0 = QtWidgets.QLabel(d)
        tb0.move(x0+130, y0)
        tb0.setText("To [s]")

        channels = [1,2,0,4,5]
        cbs, t0s, t1s = [], [], []
        for i in range(5):
            cbn = QtWidgets.QCheckBox(d)
            cbn.move(x0-20, y0+25+30*i)
            cbs.append(cbn)

            tbn = QtWidgets.QLabel(d)
            tbn.move(x0, y0+25+30*i)
            tbn.setText("H-%i"%channels[i])

            t0b = QtWidgets.QDoubleSpinBox(d)
            t0b.move(x0+35,  y0+20+30*i)
            t0b.setDecimals(3)
            t0b.setMinimum(-0.5)
            t0b.setMaximum(13)
            t0b.setValue(-0.5)
            t0b.setSingleStep(0.001)
            t0s.append(t0b)

            t1b = QtWidgets.QDoubleSpinBox(d)
            t1b.move(x0+130,  y0+20+30*i)
            t1b.setDecimals(3)
            t1b.setMinimum(-0.5)
            t1b.setMaximum(13)
            t1b.setValue(13)
            t1b.setSingleStep(0.001)
            t1s.append(t1b)

        def clear_corrections():
            print("Removing corrections...")
            for channel, cb, t0, t1 in zip(self.corrections, cbs, t0s, t1s):
                if cb.checkState() == 0:
                    continue
                i_to_drop = []
                for i, correction in enumerate(self.corrections[channel]):
                    if t0.value() <= correction.t <= t1.value():
                        i_to_drop.append(i)
                for i in i_to_drop[::-1]:
                    self.remove_correction(channel,
                                           self.corrections[channel][i],
                                           self.fjpt_parameters[channel].childs[i],
                                           False)
            self.update_corrections()
            self.draw()
            d.close()
            pass

        x0 = 30

        ok.move(x0+55,y0+180)
        ok.clicked.connect(clear_corrections)

        ca = QtWidgets.QPushButton("Cancel",d)
        ca.move(x0+82+55,y0+180)
        ca.clicked.connect(d.close)


        d.setWindowTitle("Clear corrections")
        d.setWindowModality(QtCore.Qt.ApplicationModal)
        d.exec_()

        pass

    def show_thomson(self):
        if self.loaded_shot is None:
            return
        self.statusbar.showMessage('Generating synthetic DCN from Thomson data in background...', msecs=20000)
        self.VTA2DCN_modeller = VTA2DCNThread(self)
        self.VTA2DCN_modeller.modelling_finished.connect(self.on_thomson_ready)
        self.VTA2DCN_modeller.start()

    def on_thomson_ready(self):
        self.statusbar.showMessage('Generated DCN from Thomson data.', msecs=5000)
        self.draw()

    def add_correction(self, channel, t, fringes=None, dt=0.001, UI_update=True):
        index_at = len(self.corrections[channel])
        if index_at > 0:
            if t < self.corrections[channel][0].t:
                index_at = 0
            elif self.corrections[channel][0].t < t < self.corrections[channel][-1].t:
                for i in range(len(self.corrections[channel])-1):
                    if self.corrections[channel][i].t < t < self.corrections[channel][i+1].t:
                        index_at = i+1

        #self.NN_modeller.start()


        if fringes is not None:
            fjs = [fj for fj in self.corrections[channel]]
            pms = [fjpt for fjpt in self.fjpt_parameters[channel]]
            ts = [fj.t for fj in self.corrections[channel]]
            dts = [fj.dt for fj in self.corrections[channel]]
            types = [fj.type for fj in self.corrections[channel]]

            for fj, pm, t0, dt0, typ in zip(fjs, pms, ts, dts, types):
                if typ == 'fringe_jump' \
                and pm.children()[0].value() <= t <= pm.children()[0].value()+pm.children()[1].value():
                    #print('updating fj for', t, 'at', pm.children()[0].value())
                    pm.children()[2].setValue(pm.children()[2].value()+fringes)
                    #self.update_correction(channel, fj, pm, 'fringes', Bunch(value=lambda : fj.fringes+fringes))
                    return
            to_insert = Bunch(t=t, fringes=fringes, dt=dt, type='fringe_jump')
            self.corrections[channel].insert(index_at, to_insert)
            p = [{'decimals': 4,
              'name': 'Time [s]',
              'step': 0.001,
              'type': 'float',
              'value': t},
             {'decimals': 4,
              'name': 'Duration [s]',
              'step': 0.001,
              'type': 'float',
              'value': dt},
             {'name': 'Fringes ', 'step': 1, 'type': 'int', 'value': fringes},
             {'name': 'Remove', 'type': 'action'}]
            t = Parameter.create(name='Fringe Jump %5.3fs'%(t), type="group", children=p, showTop=False)
        else:
            #embed()
            to_insert = Bunch(t=t, dt=dt, type='excluded_interval')
            self.corrections[channel].insert(index_at, to_insert)
            p = [
                {'name': 'Time [s]',
                'type': 'float',
                'value': t,
                'step': 0.001,
                'decimals':4},
                {'name': 'Duration [s]',
                'type': 'float',
                'value': dt,
                'step': 0.001,
                'decimals':4},
                {'name':'Remove',
                'type':'action'}
                ]
            t = Parameter.create(name='Excluded Interval %5.3fs+%5.3fs'%(t, dt), type="group", children=p, showTop=False)

        for a in t.childs:
            if a.type() == 'action':
                a.sigActivated.connect(partial(self.remove_correction, channel=channel,
                obj_crrctn=to_insert, obj_pt = t))
            elif a.type() == 'float':
                if 'Time' in a.name():
                    a.sigValueChanged.connect(
                        partial(self.update_correction, channel=channel, obj_crrctn=to_insert,
                            value_type='t', value_source=a))
                elif 'Duration' in a.name():
                    a.sigValueChanged.connect(
                        partial(self.update_correction, channel=channel, obj_crrctn=to_insert,
                            value_type='dt', value_source=a))
            elif a.type() == 'int' and 'Fringes' in a.name():
                a.sigValueChanged.connect(
                    partial(self.update_correction, channel=channel, obj_crrctn=to_insert,
                        value_type='fringes', value_source=a))

        #embed()
        try:
            self.fjpt_parameters[channel].insertChild(index_at, t)
        except Exception as e:
            pass # print e

        if UI_update:
            self.update_corrections()
            self.draw()
        #if UI_update: self.update_corrections_UI()

    def remove_correction(self, channel=None, obj_crrctn=None, obj_pt=None, UI_update=True):
        if channel is None or obj_crrctn is None or obj_pt is None:
            return

        try:
            self.corrections[channel].pop(self.corrections[channel].index(obj_crrctn))
            self.fjpt_parameters[channel].removeChild(obj_pt) #self.fjpt_parameters[channel].childs[index])
        except Exception as e:
            traceback.print_exc()
            embed()
        if UI_update:
            self.update_corrections()
            self.draw()
            #self.update_corrections_UI()

    def update_correction(self, channel=None, obj_crrctn=None, obj_pt=None,
                          value_type=None, value_source=None):
        if channel is None or obj_crrctn is None:
            return
        index_at = self.corrections[channel].index(obj_crrctn)
        value = value_source.value()
        if value_type == 't':
            self.corrections[channel][index_at].t = value
        elif value_type == 'fringes':
            self.corrections[channel][index_at].fringes = value
        elif value_type == 'dt':
            self.corrections[channel][index_at].dt = value
        #embed()
        self.update_corrections()
        self.draw()

    def update_corrections(self):
        if self.data is None or self.FJcorrection == {}:
            return
        t = self.data[0].time
        for i in range(5):
            self.FJcorrection[i] *= 0
            for j, FJ in enumerate(self.corrections[i]):
                if FJ.type != 'fringe_jump':
                    continue
                to_add = interp1d([t[0], FJ.t, FJ.t+FJ.dt,      t[-1]],
                                  [   0,    0, FJ.fringes, FJ.fringes])(t)
                self.FJcorrection[i] += to_add

    def new_from_CON(self, shot=None):
        return self.new(source='CON')
        #self.new(shot=shot, from_DCS=True)

    def new_from_DCK(self, shot=None):
        d = QtWidgets.QDialog()
        ok = QtWidgets.QPushButton("OK",d)
        x0, y0 = 10, 40

        exb = QtWidgets.QLineEdit(d)
        exb.setFixedWidth(80)
        exb.move(x0, y0-30)
        exb.setText(self.settings.corrections_load_exp)

        shb = QtWidgets.QSpinBox(d)
        shb.setFixedWidth(70)
        shb.move(x0+90, y0-30)
        shb.setMaximum(999999)
        shb.setValue(self.settings.last_shot)

        edb = QtWidgets.QSpinBox(d)
        #edb.setFixedWidth(20)
        edb.move(x0+170, y0-30)
        edb.setMaximum(999)

        def load_DCK():
            exp = str(exb.text())
            shot = shb.value()
            ed = edb.value()
            self.new(shot=shot, source='DCK', exp=exp, ed=ed)
            d.close()
            self.settings.corrections_load_exp = exp

        ok.move(x0+55,y0)
        ok.clicked.connect(load_DCK)

        ca = QtWidgets.QPushButton("Cancel",d)
        ca.move(x0+82+55,y0)
        ca.clicked.connect(d.close)

        d.setWindowTitle("Load DCK correction")
        d.setWindowModality(QtCore.Qt.ApplicationModal)
        d.exec_()
        return

    def new(self, shot=None, source='DCN', exp='AUGD', ed=0):
        value = self.loaded_shot if self.loaded_shot is not None else self.settings.last_shot
        if source in ('DCN', 'CON'):
            if shot is None or shot == False:
                shot, ok = QtWidgets.QInputDialog.getInt(self,
                    "New from %s"%source, "Enter a shotnumber:", value=value)
                if not ok:
                    return


        self.FJcorrection = {}
        self.corrections = {i:[] for i in range(5)}
        self.vta_modelled = None
        self.loaded_shot = None
        self.pellet_trace = None
        self.pellet_times = None
        self.NN_estimate = None
        self.brt1_trace = None

        for i in self.fjpt_parameters.keys():
            self.fjpt_parameters[i] = Parameter.create(name='Corrections', type='group')
            self.fjpt[i].setParameters(self.fjpt_parameters[i], showTop=False)

        self.update_corrections()
        self.draw()
        #self.update_corrections_UI()

        if source in ('DCN', 'DCK'):
            dcn = SFREAD(source, shot, experiment=exp, edition=ed)
            data = {}
            channels = self.channels
            if 'H-3' in dcn.getlist():
                channels[2] = 3
            else:
                channels[2] = 0            
            self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabH3), "H-%i"%channels[2])
            self.offsets = {}
            for i in range(5):
                current_t = dcn.gettimebase('H-%i'%channels[i])
                data[i] = Bunch(data=np.array(dcn.getobject('H-%i'%channels[i])), time=current_t)
                self.offsets[i] = np.average(data[i].data[current_t < 0.02])
                data[i].data -= self.offsets[i]
                self.FJcorrection[i] = np.zeros_like(data[i].time)
        elif source == 'CON':
            con = SFREAD('CON', shot)
            signals = ['DCN_H-%i'%i for i in self.channels]
            from numba.typed import Dict
            data = Dict()
            #print(signals)
            for i, sig in zip(self.channels, signals):
                data[i] = np.array(con.getobject(sig))
            from lib.eval_dcn import eval_dcn
            res = eval_dcn(np.array(con.gettimebase('DCN_ref')), np.array(con.getobject('DCN_ref')), data, chs=self.channels)
            new_t, new_dcns = res[:2]
            new_dcns *= fringe_ne
            data = {}
            self.offsets = {}
            for i in range(5):
                data[i] =  Bunch(name='H-%i'%i, data=new_dcns[i], time=new_t)
                self.offsets[i] = np.average(data[i].data[new_t < 0.02])
                data[i].data -= self.offsets[i]
                self.FJcorrection[i] = np.zeros_like(data[i].time)

        self.data = data
        self.draw()
        self.setWindowTitle('#%i - Fringe Jump Corrector'%shot)
        self.loaded_shot = shot
        self.settings.last_shot = shot

        #embed()
        try:
            _ = SFREAD('DCK', shot)
            if _.status:
                buttonReply = QtWidgets.QMessageBox.question(self,
                    'Existing AUGD:DCK found', "Would you like to load its corrections?",
                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)
                if buttonReply == QtWidgets.QMessageBox.Yes:
                    #print('Yes clicked.')
                    self.load_correction('AUGD', shot, 0)
                else:
                    #print('No clicked.')
                    pass
        except:
            pass

    def mask_data(self, channel, tvec, data):
        for c in self.corrections[channel]:
            if c.type != 'excluded_interval':
                continue
            mask = (c.t < tvec)*(tvec < c.t+c.dt)
            data[mask] = -1
        return data

    def draw(self):
        if self.data is None or self.FJcorrection == {}:
            return
        #pws = self.plots1[5]
        #pws.clear()
        pw3 = self.plots1[5]
        pw4 = self.plots2[5]
        pw3.clear()
        pw4.clear()
        pw3.addLegend()

        if self.show_NN_detection and self.NN_estimate is not None:
            pw3.plot(self.NN_estimate.time, self.NN_estimate.data)

        for i in range(5):
            pw1 = self.plots1[i]
            pw2 = self.plots2[i]
            pw1.clear()
            pw2.clear()

            tvec = self.data[i].time

            if self.show_comparison_chords:
                for j in range(5):
                    if j == i:
                        continue
                    #pen_color = pg.intColor(j)
                    #pen_color.setAlpha(15)
                    pen_color = (255, 255, 0, 60)
                    pw1.plot(tvec, self.mask_data(j, tvec, self.data[j].data+self.FJcorrection[j]*0.572e19), pen=pen_color)
                    pw2.plot(self.data[j].time, self.FJcorrection[j], pen=pen_color)

            corrected_data = self.data[i].data+self.FJcorrection[i]*0.572e19
            corrected_data = self.mask_data(i, tvec, corrected_data)

            if self.pellet_trace is not None:
                stride = 100
                pw1.plot(self.pellet_trace.time[::stride], self.pellet_trace.data[::stride]*corrected_data.max(),
                    pen=[100,100,200,90])
                pass

            if self.brt1_trace is not None:
                stride = 1
                pw1.plot(self.brt1_trace.time[::stride], self.brt1_trace.data[::stride]*corrected_data.mean(),
                    pen=[200,100,0,90])
                pass


            pw1.plot(tvec, self.data[i].data)
            pw1.plot(tvec, corrected_data, pen=(255,0,0))

            if self.vta_modelled is not None:
                t, d = self.vta_modelled[i]
                pw1.plot(t, d, pen=(0,255,255,90))

            pw3.plot(tvec, corrected_data, pen=pg.intColor(i), name='H-%i'%self.channels[i])

            #pws.plot(self.data[i].time, self.data[i].data)
            #pw2.clear()
            pw2.plot(self.data[i].time, self.FJcorrection[i])
            pw4.plot(self.data[i].time, self.FJcorrection[i], pen=pg.intColor(i))

            for c in self.corrections[i]:
                if c.type != 'excluded_interval':
                    continue
                lr = pg.LinearRegionItem([c.t, c.t+c.dt])
                lr.setZValue(-10)
                pw2.addItem(lr)
                def updateExclusion(lr=None, c=None):
                    t, t2 = lr.getRegion()
                    dt = t2-t
                    c.t = t
                    c.dt = dt
                    self.update_corrections()
                    self.draw()
                    #self.update_corrections_UI()
                lr.sigRegionChangeFinished.connect(partial(updateExclusion, lr=lr, c=c))

    def save(self, exp = None):
        import shutil

        if self.loaded_shot is None:
            return

        if exp is None or not exp:
            exp, ok = QtWidgets.QInputDialog.getText(self,
                "Save corrected density traces", "Enter an experiment:", text=self.settings.save_exp)
        else:
            ok = True
        if ok:
            exp = str(exp)
            self.settings.save_exp = exp.upper()
            #print(exp)
        else:
            return
        # todo: preview
        # todo: save exlusion intervals, too
        shot = self.loaded_shot

        data_d = {}

        store = True

        if store:
            shutil.copyfile('DCK00000.sfh.H%i'%self.channels[2], 'DCK00000.sfh')

        tvec = self.data[0].time.astype(float) # legacy, old DCKs were float so we stick to it
        data_d['time'] = tvec
        channels = self.channels
        for i, ch in enumerate(channels):
            param_d = {}
            try:
                final_data = self.data[i].data+self.FJcorrection[i]*0.572e19
                final_data = self.mask_data(i, tvec, final_data)

                data_d['H-%i'%ch] = final_data 

                param_d['Offset'] = np.clip(float(self.offsets[i]), -3e19, 3e19)
                n_FJs = len([c for c in self.corrections[i] if c.type == 'fringe_jump'])
                n_EIs = len([c for c in self.corrections[i] if c.type == 'excluded_interval'])

                param_d['N_Fehler']  = np.array([n_FJs]).astype(np.int32)

                if n_FJs > 0:
                    float_buf = np.zeros(200, dtype=np.float32)
                    int_buf = np.zeros(200, dtype=np.int32)
                    
                    parameter = 'T_Fehler'
                    float_buf[:n_FJs] = np.array(
                        [c.t for c in self.corrections[i] if c.type == 'fringe_jump']).astype(np.float32)
                    param_d[parameter] = copy.copy(float_buf)
                                        
                    parameter = 'Fehler'
                    int_buf[:n_FJs] = np.array(
                        [c.fringes for c in self.corrections[i] if c.type == 'fringe_jump']).astype(np.int32)
                    param_d[parameter] = copy.copy(int_buf)

                    parameter = 'DT_Fehle'
                    float_buf[:n_FJs] = np.array(
                        [c.dt for c in self.corrections[i] if c.type == 'fringe_jump']).astype(np.float32)
                    param_d[parameter] = copy.copy(float_buf)

                if n_EIs > 0:
                    float_buf = np.zeros(200, dtype=np.float32)
                    parameter = 'T_Exclud'
                    float_buf[:n_EIs] = np.array(
                        [c.t for c in self.corrections[i] if c.type == 'excluded_interval']).astype(np.float32)
                    param_d[parameter] = copy.copy(float_buf)


                    parameter = 'DT_Exclu'
                    float_buf[:n_EIs] = np.array(
                        [c.dt for c in self.corrections[i] if c.type == 'excluded_interval']).astype(np.float32)
                    param_d[parameter] = copy.copy(float_buf)
               
                data_d['ParH-%i'%ch] = param_d

            except Exception as e:
                traceback.print_exc()
                #embed()
        print('Saving %s:DCK:%i ...'%(exp, shot))
        if store:
            write_sf(shot, data_d, '.', 'DCK', exp=exp) 
        else:
            embed()
        print('DCK saved!')

    def close_application(self):
        self.close()

def main():
    app = QtWidgets.QApplication(sys.argv)  # A new instance of QApplication
    form = App()                        # We set the form to be our App (main_window)
    form.show()                         # Show the form
    sys.exit(app.exec_())                         # and execute the app


if __name__ == '__main__':              # if we're running file directly and not importing it
    main()                              # run the main function
